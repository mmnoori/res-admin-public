import {notifPublicKey, postRequest, consts, log, URLS} from './config/config'

const URL = '/service-worker-custom.js'


export async function registerSW() {

    if ('serviceWorker' in navigator) {

        console.log('SW is available')

        let registration = await navigator.serviceWorker.getRegistration(URL)


        if (!registration) { // if serviceWorker was not registered before
            console.log('No registration exists, registering SW...')
            registration = await navigator.serviceWorker.register(URL, {scope: '/'})
        } else {
            console.log('SW was registered, updating registration...')

            await registration.update()
        }

        await navigator.serviceWorker.ready

    } else
        console.log('SW not available')

}


export async function subscribePush() {

    let issue = false


    await registerSW()




    if (!("Notification" in window)) {
        return console.log("This browser does not support desktop notification")

    } else if (Notification.permission === "granted") {
        console.log('Notification permission granted')

    } else if (Notification.permission === "default") {
        await Notification.requestPermission((permission) => {
            if (permission === 'denied') return issue = true
        });
    } else if (Notification.permission === 'denied') {
        // can't request permission in this situation
        return console.log('Notification permission denied')
    }

    if (issue) return



    if ('serviceWorker' in navigator) {

        const registration = await navigator.serviceWorker.getRegistration(URL)


        let subscription = await registration.pushManager.getSubscription().catch(e => console.log(e))

        if (!subscription) {

            console.log('subscription did NOT exist');

            subscription = await registration.pushManager.subscribe({
                userVisibleOnly: true,
                applicationServerKey: urlBase64ToUint8Array(notifPublicKey)
            }).catch(e => console.log(e))
        }



        if (!registration.pushManager) return console.log('push unsupported')

        if (!subscription) return console.log('could not subscribe SW')

        // console.log('push Subscription expiration : ');
        // console.log(subscription.expirationTime);


        postRequest(URLS.ROUTE_REGISTER_PUSH, {subscription}, (res, statusCode) => {
            if (statusCode === consts.SUCCESS_CODE) console.log('subscribed to server')
        })


    }

}

function urlBase64ToUint8Array(base64String) {
    const padding = '='.repeat((4 - base64String.length % 4) % 4)
    const base64 = (base64String + padding)
        .replace(/\-/g, '+')
        .replace(/_/g, '/')

    const rawData = window.atob(base64)
    const outputArray = new Uint8Array(rawData.length)

    for (let i = 0; i < rawData.length; ++i) {
        outputArray[i] = rawData.charCodeAt(i)
    }
    return outputArray
}


export function unregister() {

    if ('serviceWorker' in navigator) {

        // promise waits for serviceWorker to be ready, this means it can wait until we register service worker
        // navigator.serviceWorker.ready.then(registration => {
        // registration.unregister();
        //
        // console.log('Unregistered service worker')
        // });

        navigator.serviceWorker.getRegistrations().then(function(registrations) {
          for(let registration of registrations) {
              registration.unregister()
              console.log('Unregistered SW (for loop)')
          } })

    } else {
      console.log('SW NOT in navigator')
    }

}
