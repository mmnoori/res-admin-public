import React, { Component }  from 'react' // just to be able to use functions in other components

export const isDevelopment = (process.env.NODE_ENV === 'development')

/// when we define proxy in package.json in development and production hostname will be added to first part of urls
// export const baseAddress = isDevelopment ? 'http://127.0.0.1:4050/api/' : 'http://admin.overrider.ir/api/'

export const baseAddress = '/api/'
export const imagesAddress = '/images/'
const adminPath = 'admin'

export const notifPublicKey = 'BJPx6-xQnornM0TAgKZW6FccWUfjsmtDv_a0gTM0d5RbdOXpgVYt0gbEF9EFeZY2VUEAv7PEZBkJDLc62B9bjDg'

export const URLS = {
    ROUTE_AUTHENTICATE: baseAddress + adminPath + '/authenticate',
    ROUTE_REGISTER_PUSH: baseAddress + adminPath + '/register-push',
    ROUTE_GET_STATICS: baseAddress + adminPath + '/get-statics',


    ROUTE_CHECK_TOKEN: baseAddress + adminPath + '/checkToken',
    ROUTE_CHANGE_PASS: baseAddress + adminPath + '/change-password',
    ROUTE_LOGOUT: baseAddress + adminPath + '/logout',

    ROUTE_GET_REPORTS: baseAddress + adminPath + '/get-reports',




    ROUTE_ACCEPT_ORDER_STATUS: baseAddress + adminPath + '/get-accept-order',


    ROUTE_MANAGEMENT: baseAddress + adminPath + '/management',
    ROUTE_UPDATE_MANAGEMENT: baseAddress + adminPath + '/update-management',

    ROUTE_PAY_PANEL: baseAddress + adminPath + '/pay',

    ROUTE_GET_CUSTOMERS: baseAddress + adminPath + '/customer-list',
    ROUTE_GET_COMMENTS: baseAddress + adminPath + '/comment-list',

    ROUTE_GET_CUSTOMER_INFO: baseAddress + adminPath + '/customer-info',
    ROUTE_BAN_CUSTOMER: baseAddress + adminPath + '/ban-customer',


    ROUTE_NEW_DISCOUNT_CODE: baseAddress + adminPath + '/new-discount',

    ROUTE_GET_ORDERS: baseAddress + adminPath + '/order-list',

    ROUTE_UPDATE_FOOD: baseAddress + adminPath + '/update-food',
    ROUTE_DELETE_FOOD: baseAddress + adminPath + '/delete-food',
    ROUTE_FOOD_GROUP_DISCOUNT: baseAddress + adminPath + '/food-group-discount',


    ROUTE_GET_EVENTS: baseAddress + '/event-list',

    ROUTE_UPDATE_EVENT: baseAddress + adminPath + '/update-event',
    ROUTE_DELETE_EVENT: baseAddress + adminPath + '/delete-event',

    ROUTE_UPDATE_MEALS: baseAddress + adminPath + '/update-meals',
    ROUTE_ACCEPT_ORDER: baseAddress + adminPath + '/accept-order',

    ROUTE_ADD_FOOD_LABEL: baseAddress + adminPath + '/add-food-label',
    ROUTE_EDIT_FOOD_LABEL: baseAddress + adminPath + '/edit-food-label',
    ROUTE_DELETE_FOOD_LABEL: baseAddress + adminPath + '/delete-food-label',

    ROUTE_HANDLE_ORDER: baseAddress + adminPath + '/handle-order',


    ROUTE_GET_FOODS: baseAddress + '/food-list',

    ROUTE_LIST_VERSIONS: baseAddress +  'list-versions',



}

export const consts = {
    ORDERS_CHUNK_COUNT: 20,
    CUSTOMERS_CHUNK_COUNT: 40,
    REPORTS_CHUNK_COUNT: 15,
    EVENTS_CHUNK_COUNT: 8,
    COMMENTS_CHUNK_COUNT: 15,

    SORTBAR_SORT_BY: 'به ترتیب :',
    SORTBAR_STATUS: 'وضعیت :',
    SORTBAR_LABEL: 'دسته :',

    LATEST: 'جدیدترین',
    HIGHEST_SUM: 'بالاترین مبلغ',

    HIGHEST_INVITES: 'بالاترین دعوت',
    HIGHEST_ORDERS: 'بالاترین سفارش',

    AVAILABLE: 'موجود',
    NOT_AVAILABLE: 'ناموجود',

    ALL: 'همه',
    DELIVERED: 'تحویل شده',
    COMMITTED: 'ثبت شده',
    REJECTED: 'لغو شده',
    ACCEPTED: 'قبول شده',
    SENT: 'ارسال شده',

    ONLINE: 'ONLINE',
    CASH: 'CASH',


    PAYMENT_CALLBACK: '/payment-callback',


    VALID: 'معتبر',
    BORN_TODAY: 'متولد امروز',
    BANNED: 'مسدود شده',

    SELECT_DATE: 'انتخاب روز',
    MONTH: 'ماه',
    YEAR: 'سال',
    SUGGESTED: 'پیشنهادی',

    FOOD_GROUPS: 'دسته های غذایی',

    ACTIVE_ORDERS: 'فعال',

    DAY: 'to',
    OVERALL: 'ov',


    FARVARDIN: 'فروردین',
    ORDIBEHESHT: 'اردیبهشت',
    KHORDAD: 'خرداد',
    TIR: 'تیر',
    MORDAD: 'مرداد',
    SHAHRIVAR: 'شهریور',
    MEHR: 'مهر',
    ABAN: 'آبان',
    AZAR: 'آذر',
    DEY: 'دی',
    BAHMAN: 'بهمن',
    ESFAND: 'اسفند',



    SUBMIT: 'تایید',
    SAVE: 'ذخیره',
    NEXT: 'بعدی',

    TRUE: 'TRUE',
    FALSE: 'FALSE',
    OFF: 'OFF',
    ON: 'ON',

    ADD_FOOD: 'ADD_FOOD',
    EDIT_FOOD: 'EDIT_FOOD',
    DELETE_FOOD: 'DELETE_FOOD',

    CODE_CHECKED: 'Checked Token :)',


    MODAL_ORDER: 'MODAL_ORDER',
    MODAL_CUSTOMER: 'MODAL_CUSTOMER',
    MODAL_DISCOUNT: 'MODAL_DISCOUNT',
    MODAL_FOOD: 'MODAL_FOOD',
    MODAL_EVENT: 'MODAL_EVENT',

    MODAL_FOOD_GROUP_DISCOUNT: 'MODAL_FOOD_GROUP_DISCOUNT',
    MODAL_LABELS: 'MODAL_LABELS',
    MODAL_MEALS: 'MODAL_MEALS',
    MODAL_CONFIRM: 'MODAL_CONFIRM',
    MODAL_INVITATION_PRICE: 'MODAL_INVITATION_PRICE',

    MODAL_ADMIN_MAP: 'MODAL_ADMIN_MAP',
    MODAL_SIMPLE_MAP: 'MODAL_SIMPLE_MAP',
    MODAL_COURIER: 'MODAL_COURIER',
    MODAL_CHANGE_PASSWORD: 'MODAL_CHANGE_PASSWORD',


    FOOD_NAME_MAX_LENGTH: 25,
    FOOD_DES_MAX_LENGTH: 120,
    FOOD_PRICE_MAX_LENGTH: 6,
    PERCENT_MAX_LENGTH: 2,
    COURIER_PRICE_MAX_LENGTH: 5,
    ADDRESS_MAX_LENGTH: 70,
    DISCOUNT_CODE_MAX_LENGTH: 7,

    EVENT_TITLE_MAX_LENGTH: 35,


    SUCCESS_CODE: 200,
    NO_CONTENT_CODE: 205,
    BAD_REQ_CODE: 400,
    UNAUTHORIZED_CODE: 401,
    NOT_FOUND_CODE: 404,
    INT_ERR_CODE: 500
}

export const log = function(message) {
    if (isDevelopment)
        console.log(message)
}

export const toType = function(obj) {
    return ({}).toString.call(obj).match(/\s([a-zA-Z]+)/)[1].toLowerCase()
}

export function getRequest(url, params, callback) {

    return new Promise(async (resolve, reject) => {

        // url must be complete http address
        // var url = new URL(url)

        console.log(params)

        Object.keys(params).forEach((key, i) => {

            if (i === 0) url += '?'
            url += key + '=' + params[key]

            // if was not last index
            if (i !== Object.keys(params).length - 1) url += '&'
        })

        console.log(url)



        let response = await fetch(url, {
            method: 'GET',

            // credentials: 'include', // Must specify this if you need to send cookies anyway
        })

        const statusCode = response.status
        const contentType = response.headers.get("content-type")

        let data

        if (contentType === 'application/json; charset=utf-8') data = await response.json().catch(e => log(e))
        else if (contentType === 'text/html; charset=utf-8') data = await response.text().catch(e => log(e))

        callback(data, statusCode)
        resolve({data, statusCode})
    })

}

export function postRequest(url, params, callback) {

    return new Promise(async (resolve, reject) => {

        // log('\n')
        // log('params :')
        // log(params)
        // log(toType(params) )

        let body
        let headers = {}

        if (toType(params) === 'formdata') body = params

        else if (toType(params) === 'object') {
            body = JSON.stringify(params)
            headers = { 'Content-Type': 'application/json' }
        }

        let response = await fetch(url, {
            method: 'POST',
            body,
            // body must be at least an empty object , otherwise we have remove headers config

            headers,
            // credentials: 'include', // Must specify this if you need to send cookies anyway
        })

        const statusCode = response.status
        const contentType = response.headers.get("content-type")

        let data

        if (contentType === 'application/json; charset=utf-8') data = await response.json().catch(e => log(e))
        else if (contentType === 'text/html; charset=utf-8') data = await response.text().catch(e => log(e))

        callback(data, statusCode)
        resolve({data, statusCode})
    })

}


export function dataURLtoFile(dataurl, filename) {
    var arr = dataurl.split(','),
        mime = arr[0].match(/:(.*?);/)[1],
        bstr = atob(arr[1]), n = bstr.length, u8arr = new Uint8Array(n)
    while(n--){
        u8arr[n] = bstr.charCodeAt(n)
    }
    return new File([u8arr], filename, {type:mime})
}

export function convertNumbersToEnglish(string) {
    return string.replace(/[\u0660-\u0669]/g, function (c) {
        return c.charCodeAt(0) - 0x0660
    }).replace(/[\u06f0-\u06f9]/g, function (c) {
        return c.charCodeAt(0) - 0x06f0
    })
}

export const handleInputChange = (componentInstance, event) => {

    let target = event.target
    let value = target.type === 'checkbox' ? target.checked : target.value
    let name = target.name

    if (value.length > target.maxLength) return // if value exceeds maxLength, no need to update

    componentInstance.setState({[name]: value })
}

export const getFormattedFoods = (foods) => {

    let  temp = ''

    for (let i = 0; i < foods.length; i++) {

        temp += foods[i].name + ' (' + foods[i].count + ')'

        if (i !== foods.length - 1) // if was not last index
            temp += ', '
    }
    return temp
}

export const getFormattedPrice = (price) => {

    price =  price.toString() // converting to String

    // if (price.length < 4) return price // if it is less than 4 digits , dont add ,
    //
    // let digits = price.length
    // price = price.slice(0, digits-3) + ',' + price.slice(digits -3, digits)
    // return price

    return price.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
}

export const monthToString = (month) => {
    switch (month) {
        case '01': return 'فروردین'
        case '02': return 'اردیبهشت'
        case '03': return 'خرداد'
        case '04': return 'تیر'
        case '05': return 'مرداد'
        case '06': return 'شهریور'
        case '07': return 'مهر'
        case '08': return 'آبان'
        case '09': return 'آذر'
        case '10': return 'دی'
        case '11': return 'بهمن'
        case '12': return 'اسفند'
    }
}

