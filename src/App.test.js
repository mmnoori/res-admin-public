import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

// This test mounts a component and makes sure that it didn’t throw during rendering.
// Tests like this provide a lot of value with very little effort so they are great as a starting point,
// and this is the test you will find in src/App.test.js.

// When you encounter bugs caused by changing components,
// you will gain a deeper insight into which parts of them are worth testing in your application.

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<App />, div);
  ReactDOM.unmountComponentAtNode(div);
});
