import React, {Component, useState} from 'react';
import "react-persian-calendar-date-picker/lib/DatePicker.css";
import DatePicker, {Calendar} from "react-persian-calendar-date-picker";

import {consts, log} from "../config/config";
import close from "../assets/icons/close.png";


/*
* gets styleClass, onDateChange, currentDate, removable
* */
class MyCalendar extends Component {

    constructor(props) {
        super(props);

        this.state = {
            text: consts.SELECT_DATE,
            calendarDropdown: false,
        };

        this.handleDate = this.handleDate.bind(this);
        this.toggleDropdown = this.toggleDropdown.bind(this);
        this.closeDropdown = this.closeDropdown.bind(this);
        this.removeDate = this.removeDate.bind(this);
    }

    componentDidMount() {
        // set currentDate
        const date = this.props.currentDate;
        if (date && date !== '') this.setState({text: date.year + '/' + date.month + '/' + date.day});

        document.addEventListener('mousedown', this.closeDropdown);
    }
    componentWillUnmount() {
        document.removeEventListener('mousedown', this.closeDropdown);
    }

    closeDropdown(event) {
        if (!this.calendarDropdown.contains(event.target)) this.setState({calendarDropdown: false});
    }

    handleDate(date) {

        // convert all to string for server's sake
        date.day += '';
        date.month += '';
        date.year += '';

        if (date.month < 10) { date.month = '0'+ date.month; }
        if (date.day < 10) { date.day = '0'+ date.day; }

        let selectedDate = date.year + '/' + date.month + '/' + date.day;

        this.setState({calendarDropdown: false, text: selectedDate});

        if (this.state.text !== selectedDate) // call callback if date is changed
            this.props.onDateChange(date);
    }

    toggleDropdown(event) {

        if (this.calendarBtn.contains(event.target) && !this.removeDateBtn.contains(event.target))
            this.setState({calendarDropdown: !this.state.calendarDropdown});
    }

    removeDate() {
        this.setState({text: consts.SELECT_DATE});

        this.props.onDateChange('');
    }

    render() {

        let calendarDropdown = '';
        let className = (this.props.styleClass)? this.props.styleClass: '';

        if (this.state.calendarDropdown)
            calendarDropdown = <Calendar selectedDay={this.state.selectedDay} onChange={this.handleDate} calendarClassName="my-calendar"/>;


        return (
            <div className='pos-rltv' ref={node => this.calendarDropdown = node}>

                <div className={"button calendar m-width-100 p-t-5 "+className} onClick={this.toggleDropdown} ref={node => this.calendarBtn = node}>

                    <img className={`remove-date ${(this.state.text !== consts.SELECT_DATE && this.props.removable === true)? '': 'dis-none'}`}
                         src={close} onClick={this.removeDate} ref={node => this.removeDateBtn = node}/>

                    {this.state.text}
                </div>

                {calendarDropdown}

            </div>
        );
    }
}

export default MyCalendar;
