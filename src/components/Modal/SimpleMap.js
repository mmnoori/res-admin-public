import React, {Component} from 'react'
import connect from "react-redux/es/connect/connect"

import LeafletMap from '../LeafletMap'
import CedarMap from '../CedarMap'

import {closeModal} from "../../store/mainActions"
import {consts, handleInputChange, log} from "../../config/config"



class AdminMap extends Component {

    constructor(props) {
        super(props)

        this.state = {
            location: props.modalRepo.location,
        }

    }

    render() {

        return (
            <div className='m-b-35'>

                <LeafletMap currentLocation={this.state.location} getLocation={this.getLocation} useMarker={true}/>
                {/*<CedarMap currentLocation={this.props.location} getLocation={this.getLocation}/>*/}

            </div>
        )
    }
}

const mapStateToProps = state => ({
    modalRepo: state.data.modalRepo,
})

export default connect(mapStateToProps, {closeModal})(AdminMap)
