import React, {useState} from 'react'
import Modal from 'react-responsive-modal'
import {useSelector, useDispatch} from 'react-redux'

import {log, consts, URLS, getFormattedFoods, toType} from '../../config/config'
import {closeSecondModal, deleteFood, deleteFoodLabel, updateManagement, deleteEvent, handleOrder, banCustomer} from "../../store/mainActions"



const SecondModal = (props) => {

    const dispatch = useDispatch()

    const {food, foodLabel, acceptOrderQuestion, event, activeOrder, customerToBan} = useSelector(state => state.data.secondModalRepo, [])

    const customerSort = useSelector(state => state.data.customerSort)
    const customerStatus = useSelector(state => state.data.customerStatus)
    const customerSearchText = useSelector(state => state.data.customerSearchText)

    const sortbarFoodStatus = useSelector(state => state.data.foodStatus)
    const sortbarFoodLabel = useSelector(state => state.data.foodLabel)
    const secondModal = useSelector(state => state.data.secondModal)

    // NOTE: can't return anything before calling all hooks
    // Error: React has detected a change in the order of Hooks called by SecondModal. This will lead to bugs and errors if not fixed.
    if (secondModal === false) return <></> // not rendering shit till secondModal is set



    function handleAction() {

        // delete food
        if (food) dispatch(deleteFood(food._id, sortbarFoodStatus, sortbarFoodLabel))

        // delete event
        else if (event) dispatch(deleteEvent(event._id))

        // delete foodlabel
        else if (foodLabel) dispatch(deleteFoodLabel(foodLabel.name, sortbarFoodStatus, sortbarFoodLabel))

        // cancel activeOrder
        else if (activeOrder) dispatch(handleOrder(activeOrder._id, consts.REJECTED, activeOrder.owner.phone, activeOrder.code))

        // ban customer
        else if (customerToBan)
            dispatch(banCustomer(customerToBan._id, !customerToBan.banned, 0, customerSort, customerStatus, customerSearchText, consts.CUSTOMERS_CHUNK_COUNT))

        // turn off acceptOrder
        else if (acceptOrderQuestion) dispatch(updateManagement({acceptOrder: consts.OFF}))
    }


    let Question

    if (food)
        Question =
            <>آیا از حذف <span className='txt-bold'>{food.name}</span> از دسته {food.label} اطمینان دارید؟</>

    else if (event)
        Question =
            <>آیا از حذف رویداد <span className='txt-bold'>{event.title}</span> اطمینان دارید؟</>

    else if (foodLabel)
        Question =
            <>آیا از حذف دسته <span className='txt-bold'>{foodLabel.name}</span> اطمینان دارید؟ (تمام غذا های موجود در این دسته حذف میشوند)</>

    else if (activeOrder)
        Question =
            <>آیا لغو سفارش <span className='txt-bold'>{activeOrder.owner.name}</span> شامل {getFormattedFoods(activeOrder.foods)} اطمینان دارید؟</>

    else if (customerToBan && customerToBan.banned === false)
        Question =
            <>آیا از مسدود کردن ثبت سفارش برای شماره <span className='txt-bold'>{customerToBan.phone}</span> اطمینان دارید؟</>

    else if (customerToBan && customerToBan.banned === true)
        Question =
            <>آیا از رفع مسدودیت ثبت سفارش برای شماره <span className='txt-bold'>{customerToBan.phone}</span> اطمینان دارید؟</>

    else if (acceptOrderQuestion)
        Question = 'سرویس ثبت سفارش بصورت خودکار در ساعات وعده ها انجام میشود. با تغییر این حالت هیچ سفارش جدیدی نمی پذیرید. آیا از تغییر حالت اطمینان دارید؟'



    return (
        <Modal open={(secondModal !== false)} onClose={() => dispatch(closeSecondModal())} showCloseIcon={false} center>

            <div className="modal-container">

                <div className="modal-head">
                    <div className="close-img" onClick={() => dispatch(closeSecondModal())}/>
                    <div className="modal-subject">{'اخطار'}</div>
                </div>

                <hr className="m-t-0 m-b-0"/>

                <div style={{padding: '17px'}}>

                <div className='t-align-c' style={{fontSize: '15px'}}>
                    {(toType(Question) === 'function')?  <Question/> : <>{Question}</> }
                </div>

                <div className='dis-flex j-content-c m-t-15'>
                    <div className="button confirm-btn red" onClick={() => dispatch(closeSecondModal())}>
                        لغو
                    </div>

                    <div className="button confirm-btn green m-r-55" onClick={handleAction}>
                        بلی
                    </div>
                </div>

                </div>

            </div>

        </Modal>
    )
}


// However, unlike connect(), useSelector() does not prevent the component from re-rendering due to its parent re-rendering, even if the component's props did not change.
// If further performance optimizations are necessary, you may consider wrapping your function component in React.memo()
export default React.memo(SecondModal)
