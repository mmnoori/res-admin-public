import React, {Component} from 'react';
import {connect} from 'react-redux';

import {handleStatusCode, safeRequest, handleModal, handleSecondModal} from "../../store/mainActions";
import food from "../../assets/icons/food.png";
import {consts, getFormattedFoods, getFormattedPrice, log, monthToString, URLS} from "../../config/config";
import avatar from "../../assets/icons/avatar-01.png";
import phone from "../../assets/icons/phone.png";
import location from "../../assets/icons/location.png";
import price from "../../assets/icons/price.png";
import motor from "../../assets/icons/motor.png";
import discount from "../../assets/icons/discount.png";
import creditSpent from "../../assets/icons/credit-spent.png";
import payable from "../../assets/icons/payable.png";
import atm from "../../assets/icons/atm.png";
import comment from "../../assets/icons/comment.png";


/*
* gets phone as props and whole customer in returning back both in modalRepo
* */
class Customer extends Component {

    componentDidMount() {

        // when we are getting back to this modal we save custom in modalRepo to prevent rerequest
        if (this.props.modalRepo.customer)
            this.setState({customer: this.props.modalRepo.customer});
        else

            this.props.safeRequest(URLS.ROUTE_GET_CUSTOMER_INFO, {phone: this.props.modalRepo.phone}, (res, statusCode) => {

                this.setState({customer: res});
            }, this.props.handleStatusCode)
    }


    render() {

        if (!this.state)
            return <div className='m-width-300 '><div className='loader medium'/></div>;

        const customer = this.state.customer;
        const modalRepo = this.props.modalRepo;

        return (
            <div className='m-width-300'>

                <span className={`order-badge bd-rd-5 flt-l cu-po ${(customer.banned === false)? 'red': 'green'}`}
                      onClick={() => this.props.handleConfirmModal(consts.MODAL_CONFIRM, {customerToBan: customer})}>
                                    {(customer.banned === false)?  'مسدود کردن': 'رفع مسدودیت'}
                            </span>

                <div className="food-order-text">
                    <img className="food-order-icon" src={phone}/>
                    { 'همراه : ' + customer.phone}
                </div>

                <div className="food-order-text">
                    <i className="food-order-icon fas fa-birthday-cake"/>
                    {'روز تولد : ' + ` ${Number(customer.birth.day)} ${monthToString(customer.birth.month)}`}
                </div>

                <div className="food-order-text">
                    <img className="food-order-icon" src={creditSpent}/>
                    اعتبار : {getFormattedPrice(customer.credit)} تومان
                </div>

                <hr/>

                <div className="food-order-text">
                    <img className="food-order-icon" src={food}/>
                    تعداد سفارش : {customer.ordersCount}
                </div>

                <div className="food-order-text">
                    <img className="food-order-icon" src={avatar}/>
                    تعداد دعوت : {customer.invitedsCount}
                </div>

                <div className="food-order-text">
                    <img className="food-order-icon" src={discount}/>
                    تعداد کد تخفیف مصرف شده : {customer.usedDiscountsCount}
                </div>

                <hr/>


                <div>آدرس ها</div>

                {
                    customer.addresses.map(address =>
                        <div className="food-order-text clickable" key={address._id}
                             onClick={() => this.props.handleModal(consts.MODAL_SIMPLE_MAP, consts.MODAL_CUSTOMER, [], {...modalRepo, customer, location: address.location})}>
                            <img className="food-order-icon" src={location}/>
                            {address.address} | {address.telePhone}
                        </div>
                    )

                }

                <hr/>

                <div className="food-order-text dir-ltr t-align-c">
                    {customer.created + ' : تاریخ عضویت'}
                </div>

            </div>
        );
    }
}

const mapStateToProps = state => ({
    modalRepo: state.data.modalRepo,

});

export default connect(mapStateToProps, {handleStatusCode, safeRequest, handleModal, handleConfirmModal: handleSecondModal})(Customer);
