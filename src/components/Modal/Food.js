import React, {Component} from 'react'
import {connect} from 'react-redux'
// import ProcessImage from 'react-imgpro'
import Resizer from 'react-image-file-resizer'

import {log, dataURLtoFile, consts, baseAddress, imagesAddress, handleInputChange} from '../../config/config'
import {showToast, updateFood, handleReqProcess} from '../../store/mainActions'

import close from "../../assets/icons/close.png"
import FoodLabelsButtons from "../FoodLabelsButtons"
import FlexBtn from "../FlexBtn"


/*
* gets food as props
* */
class Food extends Component {

    constructor(props) {
        super(props)

        let commonState = {
            labelDropdown: false,
            imgFile: null,
        }

        let label

        if (props.foodLabels.length !== 0)
            if (props.foodLabel !== consts.ALL) label = props.foodLabel // when page is on a specific foodLabel
            else label = props.foodLabels[0].name
        else
            label =  '-'

        let food = props.modalRepo.food

        if (food) // if editing food
            this.state = {
                ...commonState,
                editFood: true,
                name: food.name,
                des: food.des,
                label: food.label,
                available: food.available,
                suggested: food.suggested,
                price: food.price,
                discount: food.discount,
                imgSrc: (food.imgSrc)? food.imgSrc: '',
            }
        else    // if new food
            this.state = {
                ...commonState,
                editFood: false,
                label,
                suggested: false,
                available: true,
                name: '', // setting these input values in constructor because of react uncontrolled input warnings
                des: '',
                price: '',
                discount: '',
                imgSrc: '',
            }

        this.handleInputChange = this.handleInputChange.bind(this)
        this.onFileChange = this.onFileChange.bind(this)
        this.onProcessComplete = this.onProcessComplete.bind(this)

        this.handleSelectPic = this.handleSelectPic.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
        this.removePic = this.removePic.bind(this)
        this.toggleDropdown = this.toggleDropdown.bind(this)
        this.closeDropdown = this.closeDropdown.bind(this)
        this.handleFoodLabel = this.handleFoodLabel.bind(this)
        this.btnClick = this.btnClick.bind(this)
        this.handleKey = this.handleKey.bind(this)
    }

    componentDidMount() {
        document.addEventListener('mousedown', this.closeDropdown)
    }

    componentWillUnmount() {
        if (this.state.imgSrc !== '')
            URL.revokeObjectURL(this.state.imgSrc)

        document.removeEventListener('mousedown', this.closeDropdown)
    }

    closeDropdown(event) {
        if (!this.labelDropdown.contains(event.target) && !this.labelBtn.contains(event.target))
            this.setState({labelDropdown: false})
    }

    handleInputChange(e) { handleInputChange(this, e) }

    handleKey(e) {
        if (e.key !== 'Enter') return
        e.preventDefault() // avoid going next line in textArea
        this.handleSubmit()
    }

    handleSelectPic(e) { this.fileInput.click() }

    onFileChange(e) {

        const file = e.target.files[0]

        if (file === undefined) return

        // Assuming only image

        // var reader = new FileReader()
        // var url = reader.readAsDataURL(file)
        // reader.result is base64 format of image and is accessible like this:
        // reader.onloadend = function (e) {
        //     this.setState({
        //         imgSrc: [reader.result]
        //     })
        // }.bind(this)

        if (file.type !== 'image/jpeg') return this.props.showToast('فرمت عکس باید JPG/JPEG باشد')


        this.props.handleReqProcess(true)
        Resizer.imageFileResizer(
            file,
            500,
            500,
            'JPEG',
            40, // quality
            0, // rotation
            uri => {
                log('process complete')
                this.setState({ imgFile: uri, imgSrc: URL.createObjectURL(uri)})
                this.props.handleReqProcess(false)

            },
            'blob' // output type
        )


        // this.setState({ imgSrc: URL.createObjectURL(file)})
        // this.props.handleReqProcess(true)
    }

    onProcessComplete(src, err) {

        if (err) return log(err)

        const imgFile = dataURLtoFile(src, 'fileName.jpg')
        this.setState({ imgFile})
        this.props.handleReqProcess(false)

        log('process complete')
    }

    // remove selected pic
    removePic() {
        if (this.props.reqInProcess === true) return this.props.showToast('لطفا صبر کنید')

        URL.revokeObjectURL(this.state.imgSrc)
        this.setState({imgSrc: '', imgFile: null})
    }

    toggleDropdown(event) {
        this.setState({ labelDropdown: !this.state.labelDropdown })
    }

    handleFoodLabel(e) {
        let label = e.currentTarget.textContent
        this.setState({ label, labelDropdown: false })
    }

    btnClick(e) {

        if (this.suggestedBtn.contains(e.target))
            this.setState({suggested: !this.state.suggested})

        else if (this.availableBtn.contains(e.target))
            this.setState({available: !this.state.available})
    }


    handleSubmit() {

        if (this.state.name === '' || this.state.name === undefined) return this.props.showToast('نام غذا را وارد کنید')
        if (this.state.price === '' || this.state.price === undefined) return this.props.showToast('قیمت غذا را وارد کنید')

        this.props.handleReqProcess(true)

        let food = {
            _id: (this.state.editFood)? this.props.modalRepo.food._id: null,
            name: this.state.name,
            des: this.state.des,
            label: this.state.label,
            price: this.state.price,
            discount: (this.state.discount)? this.state.discount: 0,
            available: this.state.available,
            suggested: this.state.suggested,
        }

        if (this.state.editFood)
            food.changeImg = (this.state.imgSrc === this.props.modalRepo.food.imgSrc)? false: true


        // on edit mode, if user does not change
        this.props.updateFood(food, this.state.imgFile, this.props.foodStatus, this.props.foodLabel)
    }


    render() {

        let foodLabelsBtn
        let foodLabelsStyle = {}

        if (this.state.labelDropdown)
            foodLabelsBtn = <FoodLabelsButtons handleFoodLabel={this.handleFoodLabel}/>
        else {
            foodLabelsBtn = ''
            foodLabelsStyle = { opacity: 0 }
        }


        let starClass

        if (this.state.suggested) starClass = 'yellow'
        else starClass = ''

        let circleClass
        let availableTxt

        if (this.state.available) {
            circleClass = 'green'
            availableTxt = consts.AVAILABLE
        } else {
            circleClass = 'red'
            availableTxt = consts.NOT_AVAILABLE
        }

        let foodContainer

        if (this.state.imgSrc === '')
            foodContainer =
                <>
                    <div className="btn-upload-pic" onClick={this.handleSelectPic}>انتخاب عکس</div>
                    <div className="food-select-hint">عکس های افقی با نسبت 4:3 کامل نمایش داده میشوند</div>
                </>

        else if (this.state.editFood && this.state.imgSrc === this.props.modalRepo.food.imgSrc)
            foodContainer =
                <>
                    <img className="btn-cancel-img" src={close} onClick={this.removePic}/>
                    <img className="food-pic" src={imagesAddress + this.state.imgSrc} />
                </>

        else
            foodContainer =
                <>
                    <img className="btn-cancel-img" src={close} onClick={this.removePic}/>
                    <img className="food-pic" src={this.state.imgSrc} />
                </>
        // else
        //     foodContainer =
        //         <>
        //             <img className="btn-cancel-img" src={close} onClick={this.removePic}/>
        //
        //             <ProcessImage className="food-pic"
        //                           image={this.state.imgSrc}
        //                           quality={40}
        //                           scaleToFit={{ width: 400, height: 400 }}
        //                           // cover={{ width: 246, height: 184, mode: 'horizontal_center'|'vertical_bottom'}}
        //                           processedImage={this.onProcessComplete}
        //                           getImageRef={image => {}}
        //             />
        //         </>


        return (
            <div className="food-contanier-edit">


                <input type='file' className='food-pic-input' onChange={this.onFileChange} ref={node => this.fileInput=node}/>

                <div className="food-pic-select">
                    {foodContainer}
                </div>


                <div className="food-detail">

                    <div className="food-row">

                        <input className="input width-180" type="text" onChange={this.handleInputChange} name="name" maxLength={consts.FOOD_NAME_MAX_LENGTH}
                               value={this.state.name} onKeyDown={this.handleKey}  placeholder={'چلوکباب محلی'}/>

                        <div className="food-buttons">

                            <div className="food-btn" onClick={this.btnClick} ref={node => this.suggestedBtn = node}>
                                <span className={'fa fa-star star-margin ' + starClass}/>ویژه
                            </div>

                            <div className="food-btn" onClick={this.btnClick} ref={node => this.availableBtn = node}>
                                <div className={"food-btn-icon circle " + circleClass}/>{availableTxt}
                            </div>

                        </div>

                    </div>

                    <div className="food-row m-b-0" style={{marginBottom: '0'}}>

                        <div className="food-order-text">دسته:</div>
                        <div className="input m-l-20 m-r-5 pos-rltv btn-label-drop"
                             style={{ paddingLeft: '20px', minWidth: '130px', textAlign: 'center' }}
                             ref={node => this.labelBtn = node} onClick={this.toggleDropdown}>{this.state.label}</div>

                        <div className="sortbar-dropdown" ref={node => this.labelDropdown = node} style={{...foodLabelsStyle, textAlign: 'right', right: '40px',top: '30px'}}>
                            {foodLabelsBtn}
                        </div>


                        <div className="food-order-text ">قیمت:</div>
                        <input className="input m-r-5 m-l-15  width-100 dir-ltr t-align-r" type="number" maxLength={consts.FOOD_PRICE_MAX_LENGTH}
                               onChange={this.handleInputChange} name="price" value={this.state.price} onKeyDown={this.handleKey} placeholder={'32000 (تومان)'}/>

                        <div className="food-order-text ">تخفیف:</div>
                        <input className="input m-r-5 width-75 dir-ltr t-align-r" type="number" maxLength={consts.PERCENT_MAX_LENGTH}
                               onChange={this.handleInputChange} name="discount" value={this.state.discount} onKeyDown={this.handleKey} placeholder={'10 (درصد)'}/>
                    </div>

                    <hr/>

                    <div className="food-row" style={{marginBottom: '0'}}>
                        <div className="food-order-text ">توضیحات:</div>
                        <textarea className="input m-r-5 m-l-10 width-full" onChange={this.handleInputChange} name="des" maxLength={consts.FOOD_DES_MAX_LENGTH}
                                  value={this.state.des} onKeyDown={this.handleKey}
                                  placeholder={'برنج محلی 150 گرم، گوشت فیله گوساله 2 سیخ 280 گرم، رب انار، گردو، آب انار'}/>
                    </div>

                </div>

                <FlexBtn handleSubmit={this.handleSubmit}/>

            </div>
        )
    }
}

const mapStateToProps = state => ({
    foodLabels: state.data.foodLabels,
    foodLabel: state.data.foodLabel,
    foodStatus: state.data.foodStatus,
    modalRepo: state.data.modalRepo,
    toast: state.data.toast, // just to detect handle server failure responses
})

export default connect(mapStateToProps, {showToast, updateFood, handleReqProcess})(Food)
