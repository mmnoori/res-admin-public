import React, {Component} from 'react'
import Modal from 'react-responsive-modal'
import {connect} from 'react-redux'

import {closeModal, handleModal} from "../../store/mainActions"
import {consts, log} from "../../config/config"

import Food from "./Food"
import Confirm from "./SecondModal"
import FoodLabels from "./FoodLabels"
import FoodsDiscount from "./FoodsDiscount"
import Discount from "./Discount"
import Meals from "./Meals"
import AdminMap from "./AdminMap"
import SimpleMap from "./SimpleMap"
import Courier from "./Courier"
import Event from "./Event"
import Customer from "./Cusomter"
import Order from "./Order"
import InvitationPrice from "./InvitationPrice"
import ChangePassword from "./ChangePassword"


/*
* gets title as props
* */
class ModalManager extends Component {

    constructor(props) {
        super(props);

        this.state = { }


        this.handleCloseModal = this.handleCloseModal.bind(this)
        this.handleBackbtn = this.handleBackbtn.bind(this)
    }

    handleBackbtn() {

        let newBackStack = []
        let modalRepo = null // null would not change previous modalRepo

        if (this.props.backStackModals.length > 1) newBackStack = this.props.backStackModals.slice(0, this.props.backStackModals.length-1)

        // log(newBackStack)

        this.props.handleModal(this.props.backStackModals[this.props.backStackModals.length-1], null, newBackStack, modalRepo)
    }

    handleCloseModal() {

        if (this.props.onClose) this.props.onClose() // closing with given function through props
        else this.props.closeModal() // closing with redux action
    }

    render() {

        const modalRepo = this.props.modalRepo
        const show = this.props.show

        let button;
        if (this.props.backStackModals.length !== 0)
            button =
                <div className='return-btn clickable' onClick={this.handleBackbtn}>
                    <div className="arrow-right"/>
                    بازگشت
                </div>
        else
            button =
                <div className="close-btn" onClick={() => this.props.closeModal()}/>

       // <div className="close-img" onClick={() => this.props.closeModal()}/>


        let title = ''
        let modal
        let bodyStyle = {padding: '17px'}
        let extraElement = ''

        if (show === consts.MODAL_FOOD) {
            title = 'غذا'
            modal = <Food/>
        }
        else if (show === consts.MODAL_CONFIRM) {
            title = 'اخطار'
            modal = <Confirm/>
        }
        else if (show === consts.MODAL_LABELS) {
            title = consts.FOOD_GROUPS
            modal = <FoodLabels/>
        }
        else if (show === consts.MODAL_FOOD_GROUP_DISCOUNT) {
            title = 'تخففی گروهی'
            modal = <FoodsDiscount/>
        }
        else if (show === consts.MODAL_MEALS) {
            title = 'وعده ها'
            modal = <Meals/>
        }
        else if (show === consts.MODAL_DISCOUNT) {
            title = 'ارسال کد تخفیف'
            modal = <Discount/>
        }
        else if (show === consts.MODAL_ADMIN_MAP) {
            title = 'موقعیت رستوران'
            modal = <AdminMap/>
            bodyStyle = {padding: '10px 17px'}
        }
        else if (show === consts.MODAL_SIMPLE_MAP) {
            title = 'نقشه'
            modal = <SimpleMap/>
            bodyStyle = {padding: '10px 17px'}
        }
        else if (show === consts.MODAL_COURIER) {
            title = 'هزینه پیک'
            modal = <Courier/>
        }
        else if (show === consts.MODAL_EVENT) {
            title = 'رویداد'
            modal = <Event/>
        }
        else if (show === consts.MODAL_CUSTOMER) {
            title = this.props.modalRepo.name
            modal = <Customer/>
        }
        else if (show === consts.MODAL_ORDER) {
            title = 'جزئیات سفارش ' + this.props.modalRepo.order.code
            modal = <Order/>
            extraElement = <span className={`order-badge bd-rd-5 flt-l ${(modalRepo.order.state === consts.REJECTED)? 'red': 'green'}`}>{modalRepo.order.state}</span>
        }
        else if (show === consts.MODAL_INVITATION_PRICE) {
            title = 'طرح افزایش مشتری'
            modal = <InvitationPrice/>
        }
        else if (show === consts.MODAL_CHANGE_PASSWORD) {
            title = 'تغییر رمز عبور'
            modal = <ChangePassword/>
        }


        return (
            // open attribute => true opens modal, false closes modal
            // onClose is executed by module itself
            // react-responsive-modal detects outside click itself and executes onClose

            // open prop must be boolean
            <Modal open={(this.props.show !== false)} onClose={this.handleCloseModal} showCloseIcon={false} styles={{modal: {maxWidth: '1000px'}}} center>

                <div className="modal-container">

                    <div className="modal-head">
                        {button}
                        <div className="modal-subject">{title}</div>
                        {extraElement}
                    </div>

                    <hr className="m-t-0 m-b-0"/>

                    <div style={bodyStyle}>

                        {modal}

                    </div>
                </div>
            </Modal>
        )
    }
}

const mapStateToProps = state => ({
    show: state.data.showModal,
    backStackModals: state.data.backStackModals,
    modalRepo: state.data.modalRepo
})

export default connect(mapStateToProps, {closeModal, handleModal})(ModalManager)
