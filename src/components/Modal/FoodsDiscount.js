import React, {Component} from 'react';
import {connect} from 'react-redux';

import {showToast, groupFoodDiscount, handleReqProcess} from "../../store/mainActions";
import FoodLabelsButtons from "../FoodLabelsButtons";
import {consts, handleInputChange} from "../../config/config";
import FlexBtn from "../FlexBtn";

class FoodsDiscount extends Component {

    constructor(props) {
        super();

        this.state = {
            labelDropdown: false,
            label: consts.ALL,
            discount: '',
            suggestedOnly: false,
            availableOnly: false,
        };

        this.showFoodLabels = this.showFoodLabels.bind(this);
        this.handleFoodLabel = this.handleFoodLabel.bind(this);

        this.closeDropdown = this.closeDropdown.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidMount() {
        document.addEventListener('mousedown', this.closeDropdown);
    }
    componentWillUnmount() {
        document.removeEventListener('mousedown', this.closeDropdown);
    }

    closeDropdown(event) {
        if (!this.labelDropdown.contains(event.target))
            this.setState({labelDropdown: false});
    }

    handleInputChange(e) { handleInputChange(this, e) }

    showFoodLabels() {
        this.setState({labelDropdown: true})
    }

    handleFoodLabel(e) {

        let label = e.currentTarget.textContent;
        this.setState({ label, labelDropdown: false });
    }

    handleSubmit() {
        if (this.state.discount === '' || this.state.discount === undefined) return this.props.showToast('میزان تخفیف را وارد کنید');

        this.props.handleReqProcess(true);

        this.props.groupFoodDiscount(this.state.label, this.state.discount, this.state.suggestedOnly, this.state.availableOnly, this.props.foodStatus, this.props.foodLabel);
    }


    render() {

        let foodLabelsBtn;
        let foodLabelsStyle = {};

        if (this.state.labelDropdown)
            foodLabelsBtn = <FoodLabelsButtons handleFoodLabel={this.handleFoodLabel}/>;
         else {
            foodLabelsBtn = '';
            foodLabelsStyle = { opacity: 0 };
        }

        return (
            <div className='padding-10'>

                <div className='dis-flex m-b-25'>

                    <div className="food-order-text">دسته:</div>
                    <div className="input m-l-30 m-r-5 btn-label-drop" onClick={this.showFoodLabels}>{this.state.label}</div>

                    <div className="sortbar-dropdown" ref={node => this.labelDropdown = node} style={{...foodLabelsStyle, textAlign: 'right', left: '153px',top: '99px'}} >
                        <button className="sortbar-dropdown-item" onClick={this.handleFoodLabel}>{consts.ALL}</button>
                        {foodLabelsBtn}
                    </div>

                    <div className="food-order-text ">تخفیف:</div>
                    <input className="input m-r-5 width-75" type="number" maxLength={consts.PERCENT_MAX_LENGTH} onChange={this.handleInputChange} name="discount"
                           value={this.state.discount} placeholder={'10 (درصد)'}/>

                </div>

                <div className='dis-flex m-b-15 '>
                    <input className="customer-checkbox m-l-5" type="checkbox" name="suggestedOnly" value={this.state.suggestedOnly}
                           onChange={this.handleInputChange}/>
                    فقط ویژه
                </div>

                <div className='dis-flex m-b-45 '>
                    <input className="customer-checkbox m-l-5" type="checkbox" name="availableOnly" value={this.state.availableOnly}
                           onChange={this.handleInputChange}/>
                    فقط موجود
                </div>

                <FlexBtn handleSubmit={this.handleSubmit}/>

            </div>
        );
    }
}

const mapStateToProps = state => ({

    foodLabel: state.data.foodLabel,
    foodStatus: state.data.foodStatus,
});

export default connect(mapStateToProps, {showToast, groupFoodDiscount, handleReqProcess})(FoodsDiscount);
