import React, {Component} from 'react'
import connect from "react-redux/es/connect/connect"

import LeafletMap from '../LeafletMap'
import CedarMap from '../CedarMap'


import {closeModal, updateManagement} from "../../store/mainActions"
import {consts, handleInputChange, log} from "../../config/config"



class AdminMap extends Component {

    constructor(props) {
        super(props)

        this.state = {
            address: props.address,
            phone: props.phone,
        }

        this.getLocation = this.getLocation.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleKey = this.handleKey.bind(this)
        this.handleInputChange = this.handleInputChange.bind(this)
    }

    getLocation(coordinates) {
        this.setState({coordinates})
    }

    handleInputChange(e) { handleInputChange(this, e) }

    handleKey(e) {
        if (e.key !== 'Enter') return
        this.handleSubmit()
    }

    handleSubmit() {
        if ((this.state.coordinates === undefined || this.state.coordinates === null)
            && this.state.address === this.props.address && this.state.phone === this.props.phone) // if nothing changed
            return this.props.closeModal()

        let location = { ...this.state.coordinates, address: this.state.address }

        this.props.updateManagement({location, phone: this.state.phone})
    }

    render() {

        return (
            <div className='m-b-35'>

                <LeafletMap currentLocation={this.props.location} getLocation={this.getLocation}/>
                {/*<CedarMap currentLocation={this.props.location} getLocation={this.getLocation}/>*/}

                <div className='column align-items-center m-t-10'>

                    <div className='dis-flex'>
                        <div className='width-75 t-align-c'>آدرس :</div>
                        <input className="input t-align-c width-full " type="text" maxLength={consts.ADDRESS_MAX_LENGTH} onChange={this.handleInputChange}
                               placeholder='آدرس' onKeyDown={this.handleKey} name="address" value={this.state.address}/>
                    </div>

                    <div className='dis-flex m-t-5'>
                        <div className='width-75 t-align-c'>تلفن :</div>
                        <input className="input t-align-c width-full dir-ltr" type="text" maxLength={consts.ADDRESS_MAX_LENGTH} onChange={this.handleInputChange}
                               placeholder='تلفن' onKeyDown={this.handleKey} name="phone" value={this.state.phone}/>
                    </div>

                </div>

                <button className="btn btn-save-food" type="button" onClick={this.handleSubmit}>
                    ذخیره
                </button>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    location: state.data.location,
    address: state.data.address,
    phone: state.data.phone,
})

export default connect(mapStateToProps, {closeModal, updateManagement})(AdminMap)
