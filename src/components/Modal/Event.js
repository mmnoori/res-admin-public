import React, {Component} from 'react'
import {connect} from 'react-redux'
// import ProcessImage from 'react-imgpro'
import Resizer from 'react-image-file-resizer'


import {log, dataURLtoFile, consts, baseAddress, imagesAddress, handleInputChange} from '../../config/config'
import {showToast, updateEvent, handleReqProcess, } from '../../store/mainActions'
import FlexBtn from "../FlexBtn"

import close from "../../assets/icons/close.png"
import MyCKEditor from "../CKEditor"


/*
* gets event as props
* */
class Event extends Component {

    constructor(props) {
        super(props)

        let commonState = {
            imgFile: null,
        }

        let event = props.modalRepo.event

        if (event) // if editing event
            this.state = {
                ...commonState,
                editEvent: true,
                title: event.title,
                content: event.content,
                imgSrc: (event.imgSrc)? event.imgSrc: '',
                notification: event.notification
            }
        else    // if new event
            this.state = {
                ...commonState,
                editEvent: false,
                title: '', // setting these input values in constructor because of react uncontrolled input warnings
                content: '',
                imgSrc: '',
                notification: false
            }

        this.handleInputChange = this.handleInputChange.bind(this)
        this.onFileChange = this.onFileChange.bind(this)
        this.onProcessComplete = this.onProcessComplete.bind(this)

        this.handleSelectPic = this.handleSelectPic.bind(this)
        this.handleNotification = this.handleNotification.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
        this.removePic = this.removePic.bind(this)
        this.handleKey = this.handleKey.bind(this)
    }

    componentWillUnmount() {
        if (this.state.imgSrc !== '')
            URL.revokeObjectURL(this.state.imgSrc)
    }

    handleInputChange(e) { handleInputChange(this, e) }

    handleKey(e) {
        if (e.key !== 'Enter') return
        e.preventDefault() // avoid going next line in textArea
        this.handleSubmit()
    }

    handleSelectPic(e) { this.fileInput.click() }

    onFileChange(e) {

        const file = e.target.files[0]

        if (file === undefined) return
        if (file.type !== 'image/jpeg') return this.props.showToast('فرمت عکس باید JPG/JPEG باشد')

        this.props.handleReqProcess(true)
        Resizer.imageFileResizer(
            file,
            500,
            500,
            'JPEG',
            40, // quality
            0, // rotation
            uri => {
                log('process complete')
                this.setState({ imgFile: uri, imgSrc: URL.createObjectURL(uri)})
                this.props.handleReqProcess(false)

            },
            'blob' // output type
        )
    }

    onProcessComplete(src, err) {

        if (err) return log(err)

        let imgFile = dataURLtoFile(src, 'fileName.jpg')
        this.setState({ imgFile})
        this.props.handleReqProcess(false)

        log('process complete')
    }

    // remove selected pic
    removePic() {
        if (this.props.reqInProcess === true) return this.props.showToast('لطفا صبر کنید')

        URL.revokeObjectURL(this.state.imgSrc)
        this.setState({imgSrc: '', imgFile: null})
    }

    handleNotification() {

        if (this.state.editEvent) return this.props.showToast('امکان تغییر اعلان برای خبر ارسال شده وجود ندارد')

        this.setState({notification: !this.state.notification})
    }


    handleSubmit() {

        if (this.state.title === '' || this.state.title === undefined) return this.props.showToast('موضوع خبر را وارد کنید')
        if (this.state.content === '' || this.state.content === undefined) return this.props.showToast('متن خبر را وارد کنید')

        this.props.handleReqProcess(true)

        let event = {
            _id: (this.state.editEvent)? this.props.modalRepo.event._id: null,
            title: this.state.title,
            content: this.state.content,
            notification: this.state.notification
        }

        if (this.state.editEvent) {
            event.changeImg = (this.state.imgSrc === this.props.modalRepo.event.imgSrc)? false: true
            if (event.changeImg === true && this.state.imgFile === null) // if removed image and did not chose any new one
                return this.props.showToast('عکس رویداد تعیین نشده است')

        } else if (this.state.imgFile === null) // if not editting and no file was chosen
            return this.props.showToast('عکس رویداد تعیین نشده است')

        // on edit mode, if user does not change
        this.props.updateEvent(event, this.state.imgFile)
    }


    render() {

        let selectPic

        if (this.state.imgSrc === '')
            selectPic =
                <>
                    <div className="btn-upload-pic" onClick={this.handleSelectPic}>انتخاب عکس</div>
                    <div className="food-select-hint">عکس های افقی با نسبت 4:3 کامل نمایش داده میشوند</div>
                </>

        else if (this.state.editEvent && this.state.imgSrc === this.props.modalRepo.event.imgSrc)
            selectPic =
                <>
                    <img className="btn-cancel-img" src={close} onClick={this.removePic}/>
                    <img className="food-pic" src={imagesAddress + this.state.imgSrc} />
                </>
        else
            selectPic =
                <>
                    <img className="btn-cancel-img" src={close} onClick={this.removePic}/>
                    <img className="food-pic" src={this.state.imgSrc} />
                </>


        return (
            <div className="food-contanier-edit">


                <input type='file' className='food-pic-input' onChange={this.onFileChange} ref={node => this.fileInput=node}/>

                <div className="food-pic-select">
                    {selectPic}
                </div>


                <div className="food-detail">

                    <div className="food-row">

                        <input className="input width-260" type="text" onChange={this.handleInputChange} name="title" maxLength={consts.EVENT_TITLE_MAX_LENGTH}
                               value={this.state.title} placeholder={'موضوع'}/>


                        <div className={`food-btn ${(this.state.notification)? 'blue': ''}`} style={{marginRight: 'auto'}} onClick={this.handleNotification}>
                            <span className='far fa-bell star-margin'/>نوتیفیکیشن
                        </div>
                    </div>

                    <div className='text'>
                        <MyCKEditor getData={(data) => this.setState({content: data})} currentData={this.state.content}/>
                    </div>

                </div>

                <FlexBtn handleSubmit={this.handleSubmit}/>

            </div>
        )
    }
}

const mapStateToProps = state => ({
    modalRepo: state.data.modalRepo,
    reqInProcess: state.data.reqInProcess,
    toast: state.data.toast, // just to detect handle server failure responses
})

export default connect(mapStateToProps, {showToast, updateEvent, handleReqProcess})(Event)
