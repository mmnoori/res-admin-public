import React, {Component} from 'react';
import {connect} from 'react-redux';

import food from "../../assets/icons/food.png";
import avatar from "../../assets/icons/avatar-01.png";
import phone from "../../assets/icons/phone.png";
import location from "../../assets/icons/location.png";
import motor from "../../assets/icons/motor.png";
import discount from "../../assets/icons/discount.png";
import creditSpent from "../../assets/icons/credit-spent.png";
import price from "../../assets/icons/price.png";
import payable from "../../assets/icons/payable.png";
import atm from "../../assets/icons/atm.png";
import comment from "../../assets/icons/comment.png";
import {consts, getFormattedFoods, getFormattedPrice, log} from "../../config/config";
import {handleModal} from "../../store/mainActions";
import Stars from "../utils/Stars";

/*
* gets order as props
* */
class Order extends Component {

    render() {

        const modalRepo  = this.props.modalRepo;
        const order  = modalRepo.order;


        let discountCode = ''

        if (order.discountCode || order.inviterCode)
            discountCode =
                <div className="food-order-text">
                    <img className="food-order-icon" src={creditSpent}/>
                    {(order.discountCode) ?
                        <>
                            کد تخفیف : {order.discountCode}
                        </>
                        :
                        <>
                            کد معرف : {order.inviterCode}
                        </>
                    }
                </div>


        return (
            <div style={{maxWidth: '460px'}}>

                <div className="food-order-text">
                    <img className="food-order-icon" src={food}/>
                    {getFormattedFoods(order.foods)}
                </div>

                <hr/>

                <div className="food-order-text clickable"
                     onClick={() => this.props.handleModal(consts.MODAL_CUSTOMER, consts.MODAL_ORDER, [], {...modalRepo, phone: order.owner.phone, name: order.owner.name})}>
                    <img className="food-order-icon" src={avatar}/>
                    {order.owner.name}
                </div>

                <div className="food-order-text">
                    <img className="food-order-icon" src={phone}/>
                    {order.owner.phone} | {order.telePhone}
                </div>

                <div className="food-order-text clickable"
                     onClick={() => this.props.handleModal(consts.MODAL_SIMPLE_MAP, consts.MODAL_ORDER, [], {...modalRepo, location: order.location})}>
                    <img className="food-order-icon" src={location}/>
                    {order.address + ' (فاصله ' + order.distance + ' متر)'}
                </div>

                <hr/>

                <div className="food-order-text">
                    <img className="food-order-icon" src={price}/>
                    قیمت کل : {getFormattedPrice(order.price)} تومان
                </div>

                <div className="food-order-text">
                    <img className="food-order-icon" src={motor}/>
                    پیک : {getFormattedPrice(order.cPrice)} تومان
                </div>

                {discountCode}

                <div className="food-order-text">
                    <img className="food-order-icon" src={discount}/>
                    تخفیفات : {getFormattedPrice(order.discounts)} تومان
                </div>

                <div className="food-order-text">
                    <img className="food-order-icon" src={creditSpent}/>
                    اعتبار مصرفی : {getFormattedPrice(order.credit)} تومان
                </div>

                <div className="food-order-text">
                    <img className="food-order-icon" src={payable}/>
                    قابل پرداخت : {getFormattedPrice(order.payable)} تومان
                </div>

                {(order.wantAtm)?
                    <div className="food-order-text">
                        <img className="food-order-icon" src={atm}/>
                        کارتخوان میخواهد
                    </div>
                    :
                    ''
                }

                <hr/>

                <Stars order={order}/>

                <hr/>

                <div className="food-order-text t-align-c" style={{direction: 'ltr'}}>
                    {order.created}
                </div>

            </div>
        );
    }
}

const mapStateToProps = state => ({
    modalRepo: state.data.modalRepo,
});

export default connect(mapStateToProps, {handleModal})(Order);
