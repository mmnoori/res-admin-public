import React, {Component} from 'react';
import connect from "react-redux/es/connect/connect";
import { updateManagement} from "../../store/mainActions";
import {consts, handleInputChange, log} from "../../config/config";

class Courier extends Component {

    constructor(props) {
        super();

        this.state = {
            ['0-1']: props.courier['0-1'],
            ['1-2']: props.courier['1-2'],
            ['2-3']: props.courier['2-3'],
            ['3-u']: props.courier['3-u'],
        };

        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleKey = this.handleKey.bind(this);
    }

    handleInputChange(e) { handleInputChange(this, e) }

    handleKey(e) {
        if (e.key !== 'Enter') return;
        this.handleSubmit();
    }

    handleSubmit() {
        // TODO: validate prices
        this.props.updateManagement({courier: this.state});
    }

    render() {
        return (
            <>

                <div className='food-meal m-b-15'>هزینه پیک بر حسب فاصله تا رستوران (تومان)</div>

                <div className='column a-items-c'>

                    <div className='dis-flex align-items-center m-b-25'>

                        <div className='width-140 t-align-c'>کم تر از 1 کیلومتر :</div>
                        <input className="input t-align-c width-100 m-r-5 m-l-5 dir-ltr" type="number" maxLength={consts.COURIER_PRICE_MAX_LENGTH} onChange={this.handleInputChange}
                               name="0-1" value={this.state['0-1']}/>
                    </div>

                    <div className='dis-flex m-b-25'>

                        <div className='width-140 t-align-c'>1 تا 2 کیلومتر :</div>
                        <input className="input t-align-c width-100 m-r-5 m-l-5 dir-ltr" type="number" maxLength={consts.COURIER_PRICE_MAX_LENGTH} onChange={this.handleInputChange}
                               name="1-2" value={this.state['1-2']}/>
                    </div>

                    <div className='dis-flex m-b-25'>

                        <div className='width-140 t-align-c'>2 تا 3 کیلومتر :</div>
                        <input className="input t-align-c width-100 m-r-5 m-l-5 dir-ltr" type="number" maxLength={consts.COURIER_PRICE_MAX_LENGTH} onChange={this.handleInputChange}
                               name="2-3" value={this.state['2-3']}/>
                    </div>

                    <div className='dis-flex m-b-45'>

                        <div className='width-140 t-align-c'>بیشتر از 3 کیلومتر :</div>
                        <input className="input t-align-c width-100 m-r-5 m-l-5 dir-ltr" type="number" maxLength={consts.COURIER_PRICE_MAX_LENGTH} onChange={this.handleInputChange}
                               onKeyDown={this.handleKey} name="3-u" value={this.state['3-u']}/>
                    </div>

                    <button className="btn btn-save-food m-b-10" type="button" onClick={this.handleSubmit}>
                        ذخیره
                    </button>

                </div>
            </>
        );
    }
}

const mapStateToProps = state => ({
    courier: state.data.courier,
});

export default connect(mapStateToProps, {updateManagement})(Courier);
