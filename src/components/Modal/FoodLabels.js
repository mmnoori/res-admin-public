import React, {Component} from 'react'
import {connect} from 'react-redux'

import FoodLabel from '../items/FoodLabel'
import {} from '../../store/mainActions'
import {consts, log} from "../../config/config"

let labels = [] // component must be unmounted for this object to reset

class FoodLabels extends Component {

    constructor(props) {
        super(props)

        this.state = {
            newLabelsCount: 0,
        }

        this.newLabel = this.newLabel.bind(this)
        this.cancelNewLabel = this.cancelNewLabel.bind(this)
    }

    newLabel() {

        let random = Math.random()

        labels.push(<FoodLabel key={random} foodLabel={{name: ''}} cancelNewLabel={this.cancelNewLabel} index={random} editable newLabel/>)

        this.setState({newLabelsCount: this.state.newLabelsCount+1})
    }

    cancelNewLabel(key) {

        for(var i = labels.length - 1; i >= 0; i--) {

            if (labels[i].key == key) labels.splice(i, 1)
        }

        this.setState({newLabelsCount: this.state.newLabelsCount-1})
    }



    render() {

        if (this.state.newLabelsCount === 0)
            labels = this.props.foodLabels.map((foodLabel, index) => (
                <FoodLabel key={foodLabel.name} foodLabel={foodLabel} index={index}/>
            ))

        return (
            <div style={{minHeight: '350px'}}>

                <div className="labels-container">

                    {labels}

                </div>

                <div className="btn-new-food" onClick={this.newLabel}>
                    دسته جدید
                </div>

            </div>
        )
    }
}

const mapStateToProps = state => ({
    foodLabels: state.data.foodLabels
})

export default connect(mapStateToProps, {})(FoodLabels)
