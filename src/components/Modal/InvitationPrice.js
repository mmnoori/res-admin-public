import React, {Component} from 'react';
import {connect} from 'react-redux';

import {showToast, updateManagement, handleReqProcess} from "../../store/mainActions";
import FoodLabelsButtons from "../FoodLabelsButtons";
import {consts, handleInputChange, log} from "../../config/config";
import FlexBtn from "../FlexBtn";

class InvitationPrice extends Component {

    constructor(props) {
        super();

        this.state = {
            credit: props.invitation.credit,
            discount: props.invitation.discount,
            minPrice: props.invitation.minPrice,
        };


        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleKey = this.handleKey.bind(this);
    }

    handleKey(e) {
        if (e.key !== 'Enter') return;
        this.handleSubmit();
    }

    handleInputChange(e) { handleInputChange(this, e) }

    handleSubmit() {
        if (this.state.credit === '' || this.state.credit === undefined) return this.props.showToast('میزان اعتبار را وارد کنید');
        if (this.state.discount === '' || this.state.discount === undefined) return this.props.showToast('میزان تخفیف را وارد کنید');
        if (this.state.minPrice === '' || this.state.minPrice === undefined) return this.props.showToast('حداقل مبلغ سفارش را وارد کنید');

        this.props.updateManagement({invitation: this.state});
    }


    render() {

        return (
            <div className='column a-items-c'>


                <div className='food-meal m-b-15'>هدیه دعوت کننده</div>

                <div className='dis-flex m-b-25'>

                    <div className='width-140 t-align-c'>اعتبار (تومان) :</div>
                    <input className="input t-align-c width-100 m-r-5 m-l-5 dir-ltr" type="number" maxLength={consts.COURIER_PRICE_MAX_LENGTH} onChange={this.handleInputChange}
                           name="credit" value={this.state.credit}/>
                </div>

                <div className='food-meal m-b-15'>تخفیف اولین سفارش کاربر دعوت شده</div>

                <div className='dis-flex m-b-25'>

                    <div className='width-140 t-align-c'>تخفیف (درصد) :</div>
                    <input className="input t-align-c width-100 m-r-5 m-l-5 dir-ltr" type="number" maxLength={consts.PERCENT_MAX_LENGTH} onChange={this.handleInputChange}
                           name="discount" value={this.state.discount}/>
                </div>

                <div className='dis-flex m-b-45'>

                    <div className='width-140 t-align-c'>حداقل سفارش (تومان) :</div>
                    <input className="input t-align-c width-100 m-r-5 m-l-5 dir-ltr" type="number" maxLength={consts.FOOD_PRICE_MAX_LENGTH} onChange={this.handleInputChange}
                           name="minPrice" value={this.state.minPrice} onKeyDown={this.handleKey}/>
                </div>


                <button className="btn btn-save-food m-b-10" type="button" onClick={this.handleSubmit}>
                    ذخیره
                </button>


            </div>
        );
    }
}

const mapStateToProps = state => ({
    invitation: state.data.invitation,
});

export default connect(mapStateToProps, {showToast, updateManagement, handleReqProcess})(InvitationPrice);
