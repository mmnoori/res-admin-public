import React, {Component} from 'react';
import connect from "react-redux/es/connect/connect";

import {consts, handleInputChange, log, postRequest, URLS} from '../../config/config';
import {closeModal, handleModal, showToast, handleStatusCode, safeRequest} from "../../store/mainActions";
import MyCalendar from '../Calendar';


/*
* gets customersToSetDiscount as props
* */
class Discount extends Component {

    constructor() {
        super();

        this.state = {
            code: '',
            expires: '',
            percent: '',
            minPrice: '',
            occasion: '',
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) { handleInputChange(this, event) }


    async handleSubmit() {

        if (this.state.code === '' || this.state.code === undefined) return this.props.showToast('کد را وارد کنید');
        if (this.state.percent === '' || this.state.percent === undefined) return this.props.showToast('درصد تخفیف را وارد کنید');
        if (this.state.expires === '' || this.state.expires === undefined) return this.props.showToast('تاریخ انقضا تعیین نشده است');

        let customers = [];
        let object = {}

        // all customers tik selected
        if (this.props.modalRepo.customers === consts.ALL) {

            customers = consts.ALL;

            if (this.props.customerSearchText !== '') object = {searchText: this.props.customerSearchText}
            if (this.props.customerStatus !== consts.VALID) object = {...object, state: this.props.customerStatus}

        }
        else
            // forEach for Map Object
            this.props.modalRepo.customers.forEach((value, key, map) => {
                if (value === true)  customers.push(key)
            });


        this.props.safeRequest(URLS.ROUTE_NEW_DISCOUNT_CODE, {...this.state, code: this.state.code.toUpperCase(), customers, ...object}, (data, statusCode) => {

            if (this.props.handleStatusCode(data, statusCode).type !== consts.CODE_CHECKED) return;

            this.props.showToast('کد تخفیف با موفقیت برای ' + data.count + ' ارسال شد');
            this.props.closeModal();
        }, this.props.handleStatusCode)
    }

    render() {

        return (
            <>
                <div className="dis-flex t-align-c dir-rtl m-b-15" >

                    <div className="food-order-text ">کد :</div>
                    <input className="input code m-r-5 m-l-15 width-100 dir-ltr t-align-r" type="text" maxLength={consts.DISCOUNT_CODE_MAX_LENGTH} onChange={this.handleChange}
                           name="code" value={this.state.code} placeholder={'GK36S'}/>


                    <div className="food-order-text ">درصد تخفیف :</div>
                    <input className="input m-r-5 m-l-15  width-100 dir-ltr t-align-r" type="number" maxLength={consts.PERCENT_MAX_LENGTH} onChange={this.handleChange}
                           name="percent" value={this.state.percent} placeholder={'20'}/>

                    <div className="food-order-text ">حداقل سفارش (تومان) (اختیاری) :</div>
                    <input className="input m-r-5 m-l-15  width-100 dir-ltr t-align-r" type="number" maxLength={consts.FOOD_PRICE_MAX_LENGTH} onChange={this.handleChange}
                           name="minPrice" value={this.state.minPrice} placeholder={'50000'}/>

                </div>

                <div className="dis-flex t-align-c dir-rtl" >

                    <div className="food-order-text ">انقضاء :</div>
                    <MyCalendar onDateChange={(date) => this.setState({expires: date})} styleClass='m-r-5 m-l-15'/>

                    <div className="food-order-text ">مناسبت (اختیاری) :</div>
                    <input className="input m-r-5 m-l-15  width-180 t-align-r" type="text" maxLength={consts.FOOD_NAME_MAX_LENGTH} onChange={this.handleChange}
                           name="occasion" value={this.state.occasion} placeholder={'روز تولد شما | سال نو'}/>

                </div>

                <hr/>

                <div className="t-align-c">
                    <div className="button m-width-100" onClick={this.handleSubmit}>تایید</div>
                </div>

            </>
        );
    }
}

const mapStateToProps = state => ({
    customerStatus: state.data.customerStatus,
    customerSearchText: state.data.customerSearchText,

    modalRepo: state.data.modalRepo,
});

export default connect(mapStateToProps, {closeModal, handleModal, showToast, handleStatusCode, safeRequest})(Discount);
