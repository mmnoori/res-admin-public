import React, {Component} from 'react';
import connect from "react-redux/es/connect/connect";
import { safeRequest, handleStatusCode, handleReqProcess, showToast, closeModal} from "../../store/mainActions";
import {consts, handleInputChange, log, URLS} from "../../config/config";
import FlexBtn from "../FlexBtn";

class ChangePassword extends Component {

    constructor(props) {
        super();

        this.state = {
            pass1: '',
            pass2: ''
        };

        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleKey = this.handleKey.bind(this);
    }

    handleInputChange(e) { handleInputChange(this, e) }

    handleKey(e) {
        if (e.key !== 'Enter') return;
        this.handleSubmit();
    }

    handleSubmit() {

        if (this.state.pass1 !== this.state.pass2) return this.props.showToast('رمز های وارد شده با هم برابر نیستند')

        this.props.handleReqProcess(true);


        this.props.safeRequest(URLS.ROUTE_CHANGE_PASS, {pass: this.state.pass1}, (data, statusCode) => {


            this.props.showToast('رمز عبور با موفقیت بروز شد')
            this.props.closeModal();
            this.props.handleReqProcess(false);

        }, this.props.handleStatusCode)

    }

    render() {
        return (
            <>

                <div className='food-meal m-b-15'>لطفا رمز عبور جدید را وارد کنید</div>

                <div className='column a-items-c'>

                    <div className='dis-flex align-items-center m-b-25'>

                        <div className='width-140 t-align-c'>رمز عبور :</div>
                        <input className="input t-align-c width-180 m-r-5 m-l-5 dir-ltr" type="password" maxLength={consts.FOOD_NAME_MAX_LENGTH} onChange={this.handleInputChange}
                               name="pass1" value={this.state.pass1} autoComplete='off'/>
                    </div>

                    <div className='dis-flex m-b-45'>

                        <div className='width-140 t-align-c'>تکرار رمز عبور :</div>
                        <input className="input t-align-c width-180 m-r-5 m-l-5 dir-ltr" type="password" maxLength={consts.FOOD_NAME_MAX_LENGTH} onChange={this.handleInputChange}
                               name="pass2" value={this.state.pass2} onKeyDown={this.handleKey} autoComplete='off'/>
                    </div>


                    <FlexBtn className='m-b-10' handleSubmit={this.handleSubmit}/>

                </div>
            </>
        );
    }
}

const mapStateToProps = state => ({
});

export default connect(mapStateToProps, {handleReqProcess, safeRequest, handleStatusCode, showToast, closeModal})(ChangePassword);
