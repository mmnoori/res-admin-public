import React, {Component} from 'react'
import {connect} from 'react-redux'

import {showToast, updateMeals,handleReqProcess, handleSecondModal} from "../../store/mainActions"
import {consts, handleInputChange, } from "../../config/config"
import FlexBtn from "../FlexBtn"

class Meals extends Component {

    constructor(props) {
        super(props)

        this.state = {
            hasTwoMeals: props.meals.hasTwoMeals,

            lsm: props.meals.lsm,
            lsh: props.meals.lsh,
            lem: props.meals.lem,
            leh: props.meals.leh,
            dsm: props.meals.dsm,
            dsh: props.meals.dsh,
            dem: props.meals.dem,
            deh: props.meals.deh,
        }

        this.handleCheckBox = this.handleCheckBox.bind(this)
        this.handleInputChange = this.handleInputChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)

    }

    handleCheckBox() {
        this.setState({...this.state, hasTwoMeals : !this.state.hasTwoMeals})
    }

    handleInputChange(e) { handleInputChange(this, e) }

    handleSubmit() {

        if (this.state.lsh === '' || this.state.lsm === '' || this.state.leh === '' || this.state.lem === ''
            || this.state.dsh === '' || this.state.dsm === '' || this.state.deh === '' || this.state.dem === '')
                    return this.props.showToast('لطفا همه فیلد های خالی را پر کنید')

        if (this.state.lsh > 23 || this.state.leh > 23 || this.state.dsh > 23|| this.state.deh > 23)
            return this.props.showToast('ساعت ورودی باید بین 0 تا 23 باشد')

        if (this.state.lsm > 59 || this.state.lem > 59 || this.state.dsm > 59|| this.state.dem > 59)
            return this.props.showToast('دقیقه ورودی باید بین 0 تا 59 باشد')

        if (Number(this.state.lsh) > Number(this.state.leh))
            return this.props.showToast('ساعت پایان وعده اول قبل از ساعت شروع خودش تعیین شده')

        if (Number(this.state.dsh) < Number(this.state.leh))
            return this.props.showToast('ساعت شروع وعده دوم قبل از ساعت پایان وعده اول تعیین شده')

        this.props.handleReqProcess(true)

        this.props.updateMeals(this.state)
    }


    render() {
        return (
            <div className='t-align-c p-l-10 p-b-10 p-r-10'>


                <div className='food-meal-con m-b-10'>

                    <div className='food-meal m-b-15'>وعده اول | نهار</div>
                    <div className='dis-flex align-items-center '>

                        <div>شروع :</div>
                        <input className="input time-input m-r-5 m-l-5" type="number" maxLength={consts.PERCENT_MAX_LENGTH} onChange={this.handleInputChange}
                               name="lsm" value={this.state.lsm} placeholder={'10'}/>
                        :
                        <input className="input time-input m-r-5" type="number" maxLength={consts.PERCENT_MAX_LENGTH} onChange={this.handleInputChange}
                               name="lsh" value={this.state.lsh} placeholder={'12'}/>


                        <div className='m-r-20'>پایان :</div>
                        <input className="input time-input m-r-5 m-l-5" type="number" maxLength={consts.PERCENT_MAX_LENGTH} onChange={this.handleInputChange}
                               name="lem" value={this.state.lem} placeholder={'00'}/>
                        :
                        <input className="input time-input m-r-5" type="number" maxLength={consts.PERCENT_MAX_LENGTH} onChange={this.handleInputChange}
                               name="leh" value={this.state.leh} placeholder={'16'}/>
                    </div>

                </div>

                <div className='food-meal-con  m-b-45'>

                    <div className='food-meal m-b-15'>

                        <div className='pos-abs'>
                            <label className="order-switch">
                                <input type="checkbox" checked={this.state.hasTwoMeals === true} onChange={this.handleCheckBox}/>
                                <span className="slider"/>
                            </label>
                        </div>
                        وعده دوم | شام
                    </div>


                    <div className='dis-flex align-items-center'>

                        <div>شروع :</div>
                        <input className="input time-input m-r-5 m-l-5" type="number" maxLength={consts.PERCENT_MAX_LENGTH} onChange={this.handleInputChange}
                               name="dsm" value={this.state.dsm} placeholder={'10'}/>
                        :
                        <input className="input time-input m-r-5" type="number" maxLength={consts.PERCENT_MAX_LENGTH} onChange={this.handleInputChange}
                               name="dsh" value={this.state.dsh} placeholder={'12'}/>


                        <div className='m-r-20'>پایان :</div>
                        <input className="input time-input m-r-5 m-l-5" type="number" maxLength={consts.PERCENT_MAX_LENGTH} onChange={this.handleInputChange}
                               name="dem" value={this.state.dem} placeholder={'00'}/>
                        :
                        <input className="input time-input m-r-5" type="number" maxLength={consts.PERCENT_MAX_LENGTH} onChange={this.handleInputChange}
                               name="deh" value={this.state.deh} placeholder={'16'}/>
                    </div>

                </div>


                <FlexBtn handleSubmit={this.handleSubmit}/>

            </div>
        )
    }
}

const mapStateToProps = state => ({
    meals: state.data.meals,
})

export default connect(mapStateToProps, {handleSecondModal, showToast, updateMeals, handleReqProcess})(Meals)
