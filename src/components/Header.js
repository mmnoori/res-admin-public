import React, {Component} from 'react'
import {connect} from 'react-redux'

import * as serviceWorker from '../serviceWorkerHandler'

import { handleSecondModal, updateManagement, fetchCustomers, fetchOrders, handleModal} from '../store/mainActions'
import avatar from '../assets/icons/avatar-01.png'
import {consts, handleInputChange, log, postRequest, URLS} from '../config/config'


class Header extends Component {

    constructor(props) {
        super(props)

        this.state = {
            searchText: ''
        }

        this.handleConfirmModal = this.handleConfirmModal.bind(this)
        this.handleKey = this.handleKey.bind(this)
        this.handleSearch = this.handleSearch.bind(this)
        this.handleLogout = this.handleLogout.bind(this)
    }

    componentDidMount() {
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
    }

    getSnapshotBeforeUpdate(prevProps, prevState) {

        // if route changes
        // because after browser refresh route in store/redux will change from '' to '/something',
        // we can count on commands inside this if to get executed first like DidMount
        if (prevProps.route !== this.props.route) {

            // wipe searchBar Text on route change
            this.setState({searchText: ''})

            // update service worker
            serviceWorker.subscribePush()
        }

        return null
    }


    handleKey(e) {
        if (e.key !== 'Enter') return
        e.preventDefault() // avoid going next line in textArea
        this.handleSearch()
    }


    handleSearch() {

        if (this.props.route === '/customers') {
            this.props.fetchCustomers(0, this.props.customerSort, this.props.customerStatus, this.state.searchText, consts.ORDERS_CHUNK_COUNT)

        } else if (this.props.route === '/orders') {
            this.props.fetchOrders(0, this.props.orderSort, this.props.orderStatus, this.props.orderDate, this.state.searchText, consts.CUSTOMERS_CHUNK_COUNT)
        }

    }

    handleConfirmModal() {

        if (this.props.automatedOrder === false) this.props.updateManagement({acceptOrder: consts.ON})
        else this.props.handleConfirmModal(consts.MODAL_CONFIRM, {acceptOrderQuestion: true})
    }

    handleLogout() {

        postRequest(URLS.ROUTE_LOGOUT, {}, (data, statusCode) => {

            if (statusCode === consts.SUCCESS_CODE) window.location.reload()
        })

    }

    render() {

        let searchBar = ''
        if (this.props.route === '/customers' || this.props.route === '/orders') {

            let placeHolder
            if (this.props.route === '/customers') placeHolder = 'کد کاربر | شماره همراه | نام'
            else if (this.props.route === '/orders') placeHolder = 'کد سفارش | شماره همراه'


            searchBar =
                <div className='row'>
                    <input className="search-input width-260" type="text" name="searchText" value={this.state.searchText} placeholder={placeHolder}
                           onKeyDown={this.handleKey} maxLength={30} onChange={(e) => handleInputChange(this, e)}/>
                    <div className="btn-search dis-flex a-items-c cu-po" onClick={this.handleSearch}>
                        <i className=" fas fa-search "/>
                    </div>
                </div>
        }


        return (
            <header className="header-desktop p-l-20 p-r-20 dir-rtl" ref={node => this.wrapperRef = node}>


                <div className='dis-flex a-items-c accept-order-con'>

                    <div className='header-accept-txt'>
                        سرویس خودکار
                    </div>

                    <label className="order-switch m-r-5">
                        <input type="checkbox" checked={this.props.automatedOrder} onChange={this.handleConfirmModal}/>
                        <span className="slider"/>
                    </label>

                </div>


                {searchBar}


                <div className="account-con">

                    <div className="dis-flex a-items-c">
                        <a className="">مدیر</a>
                        <span className='caret-down m-r-5'/>
                    </div>

                    <img className='account-img m-r-10' src={avatar} />


                    <ul className="account-dropdown">
                        <li className="" onClick={() => this.props.handleModal(consts.MODAL_CHANGE_PASSWORD)}>
                            تغییر رمز ورود
                        </li>
                        <li onClick={this.handleLogout}>
                            خروج
                        </li>
                    </ul>

                </div>


            </header>
        )
    }
}

const mapStateToProps = state => ({
    route: state.data.currentRoute,
    showModal: state.data.showModal,

    customerSort: state.data.customerSort,
    customerStatus: state.data.customerStatus,
    customerSearchText: state.data.customerSearchText,

    orderSort: state.data.orderSort,
    orderStatus: state.data.orderStatus,
    orderSearchText: state.data.orderSearchText,
    orderDate: state.data.orderDate,


    automatedOrder: state.data.automatedOrder
})

export default connect(mapStateToProps, {handleConfirmModal: handleSecondModal, updateManagement, fetchCustomers,  fetchOrders, handleModal})(Header)
