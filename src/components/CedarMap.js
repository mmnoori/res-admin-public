import React, {Component} from 'react'

import CedarMaps from '@cedarstudios/react-cedarmaps'

import placeHolder from '../assets/icons/placeholder.png'
import {log} from "../config/config"

const {RotationControl, ZoomControl, ScaleControl, Marker} = CedarMaps.getReactMapboxGl()

const token = '1d2fcb03257517b1b8b07be28f7ba0edfec32e9e'


// NOTICE: on DEVELOPMENT because of caching request with host: localhost but different ports we have CORS error
class CedarMap extends Component {

    constructor(props) {
        super(props)


        this.state = {
            center: (props.currentLocation !== undefined && props.currentLocation !== null && props.currentLocation.length !== 0)? props.currentLocation : [49.565977, 37.288467],
            loaded: false,
            zoom: 15
        }

        this.handleMoveEnd = this.handleMoveEnd.bind(this)
        this.handleRender = this.handleRender.bind(this)

    }

    handleRender(map, e) {
        // setting zoom on first load
        if (this.state.loaded === false) map.setZoom(this.state.zoom)

        this.setState({loaded: true})
    }

    handleMoveEnd(map, e) {

        if (this.state.loaded === false) return; // onMoveEnd is called in map first render

        if (this.props.getLocation)     // if had a callback function
            this.props.getLocation(map.getCenter())

    }

    render() {
        return (
            <div className='map'>
                <CedarMaps
                    containerStyle={{
                        height: '100%',
                        width: '100%',
                        direction: 'ltr',
                        borderRadius: '5px'
                    }}
                    onMoveEnd={this.handleMoveEnd}
                    onRender={this.handleRender}
                    token={token}
                    center={this.state.center}
                    // zoom={[16]}
                >

                    {/*<Marker*/}
                    {/*    coordinates={this.state.center}*/}
                    {/*    anchor="bottom">*/}
                    {/*    <img src={placeHolder}/>*/}
                    {/*</Marker>*/}
                    <div className="marker-container">
                        <img src={placeHolder} className="marker"/>
                    </div>

                </CedarMaps>

            </div>
        )
    }
}

export default CedarMap