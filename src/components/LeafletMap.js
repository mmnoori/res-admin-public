import React, {Component} from 'react'

import { Map, TileLayer, Marker, Popup } from 'react-leaflet' // must have installed leaflet and import leaflet stylesheet
import {log} from "../config/config"

import placeHolder from '../assets/icons/placeholder.png'

/*
* gets useMarker: boolean as props
* */
class LeafletMap extends Component {

    constructor(props) {
        super(props)

        this.state = {
            center: [props.currentLocation.lat, props.currentLocation.lng], // saving [lng, lat] in database
            zoom: 16
        }

        this.handleMoveEnd = this.handleMoveEnd.bind(this)
        this.handleClick = this.handleClick.bind(this)

    }

    handleMoveEnd(e) {

        if (this.props.getLocation)     // if had a callback function
            this.props.getLocation(e.target.getCenter())

        this.setState({center: e.target.getCenter(), zoom: e.target._zoom})
    }

    handleClick(e) {
        this.setState({center: [e.latlng.lat, e.latlng.lng]})
    }


    render() {


        return (
            <Map className="map" center={this.state.center} zoom={this.state.zoom} onMoveend={this.handleMoveEnd} ref={node => this.map = node}
                 maxZoom={19} minZoom={14} onClick={this.handleClick} >

                <TileLayer attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                    url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png" />

                {
                    (this.props.useMarker)?
                        <Marker position={[this.props.currentLocation.lat, this.props.currentLocation.lng]}>
                            <Popup>
                                ما اینجاییم :)
                            </Popup>
                        </Marker>
                        :
                        <div className="marker-container">
                            <img src={placeHolder} className="marker"/>
                        </div>
                }

            </Map>
        )
    }
}

export default LeafletMap
