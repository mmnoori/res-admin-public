import React, {Component} from 'react';
import {connect} from "react-redux";
import {closeModal, fetchComments, handleModal, handleReqProcess} from "../../store/mainActions";
import {consts, log, monthToString} from "../../config/config";
import Stars from "../utils/Stars";

class Comments extends Component {

    constructor() {
        super();


        this.state = {
        };

        this.handleScroller = this.handleScroller.bind(this);
        this.fetchComments = this.fetchComments.bind(this);
    }

    componentDidMount() {
        this.fetchComments(0);
    }

    handleScroller(e) {
        // if request already sent or there are no more results
        if (this.props.reqInProcess === true || this.props.commentEnd === true) return;

        if (e.target.scrollHeight - e.target.scrollTop <= 1000) {

            this.props.handleReqProcess(true);
            this.fetchComments(this.props.comments.length);
        }
    }

    fetchComments(skipCount) {
        this.props.fetchComments(skipCount, consts.COMMENTS_CHUNK_COUNT);
    }


    render() {

        return (
            <div className="container-main" onScroll={this.handleScroller}>

                <div className='row dir-rtl'>

                    {this.props.comments.map((order) =>
                        <div className='order-con'>
                            <div className='order-layout'>

                                <div className='item-head dis-flex'>

                                    <span className='order-badge gray m-r-0' style={{fontSize: '13px'}}>{order.owner.name}</span>
                                    <span className='m-r-a' style={{lineHeight: '30px'}}>{`سفارش ${order.code}`}</span>

                                </div>

                                <div className='item-body'>


                                    <Stars order={order}/>

                                    {/*<div className='title'>{order.score.comment}</div>*/}
                                    {/*<div>{order.desc}</div>*/}

                                    <hr className='m-t-a'/>

                                    <div className='dir-ltr'>{order.score.created}</div>

                                </div>

                            </div>
                        </div>
                    )
                    }


                    {(!this.props.commentEnd)?
                        <div className='width-full flt-r m-t-15 m-b-15'>
                            <div className='loader medium'/>
                        </div>
                        :
                        ''
                    }
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    comments: state.data.comments,
    commentEnd: state.data.commentEnd,

    reqInProcess: state.data.reqInProcess,
});

export default connect(mapStateToProps, {handleModal, closeModal, handleReqProcess, fetchComments})(Comments);
