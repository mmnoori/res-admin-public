import React, {Component} from 'react'
import connect from "react-redux/es/connect/connect"

import {consts, handleInputChange, log, URLS} from "../../config/config"
import {fetchManagement, handleModal,updateManagement, handleStatusCode, safeRequest} from "../../store/mainActions"

class Management extends Component {

    constructor(props) {
        super(props)

        this.state = {
            dfoEditable: false,
            dfoValue: '',
            maxDEditable: false,
            maxDValue: '',
            leastPEditable: false,
            leastPValue: '',
        }

        this.handleChange = this.handleChange.bind(this)
        this.handleOkEdit = this.handleOkEdit.bind(this)
        this.handleClick = this.handleClick.bind(this)
        this.handlePay = this.handlePay.bind(this)
    }

    componentDidMount() {
        this.props.fetchManagement()
    }

    componentWillReceiveProps(nextProps) {

        if (this.state.dfoValue === '') // update input values after fetchManagement
            this.setState({ dfoValue: nextProps.dfo, maxDValue: nextProps.maxDistance, leastPValue: nextProps.leastPrice })
    }

    handleChange(e) { handleInputChange(this, e) }

    handleClick(e) {

        if (this.locationBtn.contains(e.target)) this.props.handleModal(consts.MODAL_ADMIN_MAP)

        else if (this.courierBtn.contains(e.target)) this.props.handleModal(consts.MODAL_COURIER)

        else if (this.invitationBtn.contains(e.target)) this.props.handleModal(consts.MODAL_INVITATION_PRICE)
    }

    handleOkEdit(e) {

        // if a key was pressed and it wasn't Enter --> return
        if (e.key !== undefined && e.key !== 'Enter') return

        if (this.dfo.contains(e.target)) {
            // if it was already editable, sent to server
            if (this.state.dfoEditable) this.props.updateManagement({dfo: this.state.dfoValue})
            else this.dfoInput.focus() // if opening edit, focus

            this.setState({dfoEditable: !this.state.dfoEditable})

        } else if (this.maxD.contains(e.target)) {
            // if it was already editable, sent to server
            if (this.state.maxDEditable) this.props.updateManagement({maxDistance: this.state.maxDValue})
            else this.maxDInput.focus(); // if opening edit, focus

            this.setState({maxDEditable: !this.state.maxDEditable})

        } else if (this.leastP.contains(e.target)) {
            // if it was already editable, sent to server
            if (this.state.leastPEditable) this.props.updateManagement({leastPrice: this.state.leastPValue})
            else this.leastPInput.focus() // if opening edit, focus

            this.setState({leastPEditable: !this.state.leastPEditable})
        }
    }

    handlePay() {

        return

        this.props.safeRequest(URLS.ROUTE_PAY_PANEL, {}, (data, statusCode) => {

                window.location.href = data // redirect

        }, this.props.handleStatusCode)

    }

    render() {

        let nEditableBtnClass = "food-lbl-btn food-lbl-edit m-r-5"
        let nEditableInputClass = "food-label-input width-100 p-r-5 bd-rd-5"

        let editableBtnClass = "food-lbl-btn food-lbl-check m-r-5"
        let editableInputClass = "width-100 p-r-5 bd-rd-5 dir-ltr t-align-r"


        return (
            <div className="container-main">

                <div className='dir-rtl'>

                    <div className="row m-b-25">

                        <div className="button grey m-r-45" ref={node => this.locationBtn =node} onClick={this.handleClick}>
                            اطلاعات رستوران
                        </div>

                        <div className="button grey m-r-15" ref={node => this.courierBtn=node} onClick={this.handleClick}>
                            هزینه پیک
                        </div>

                        <div className="button grey m-r-15" ref={node => this.invitationBtn=node} onClick={this.handleClick}>
                            مدیریت دعوت ها
                        </div>

                    </div>


                    <div className='dis-flex'>

                        <div className='dis-flex manage-label-con '>

                            <div className='manage-label p-r-8'>اعتبار اولیه کاربر (تومان) :</div>

                            <div className="manage-label-input-con m-r-5"  ref={node => this.dfo = node} >
                                <input className={(this.state.dfoEditable)? editableInputClass: nEditableInputClass} type="number" name='dfoValue' maxLength={5}
                                       onChange={this.handleChange} value={this.state.dfoValue} onKeyDown={this.handleOkEdit} ref={node => this.dfoInput=node}/>
                                <div className={(this.state.dfoEditable)? editableBtnClass: nEditableBtnClass}onClick={this.handleOkEdit}/>
                            </div>

                        </div>

                        <div className='dis-flex manage-label-con m-r-50'>

                            <div className='manage-label p-r-8'>حداکثر فاصله پذیرش (متر) :</div>

                            <div className="manage-label-input-con m-r-5"  ref={node => this.maxD = node}>
                                <input className={(this.state.maxDEditable)? editableInputClass: nEditableInputClass} type="number" name='maxDValue' maxLength={4}
                                       onChange={this.handleChange} value={this.state.maxDValue} onKeyDown={this.handleOkEdit} ref={node => this.maxDInput=node}/>
                                <div className={(this.state.maxDEditable)? editableBtnClass: nEditableBtnClass} onClick={this.handleOkEdit}/>
                            </div>

                        </div>

                        <div className='dis-flex manage-label-con m-r-50'>

                            <div className='manage-label p-r-8'>حداقل مبلغ سفارش (تومان) :</div>

                            <div className="manage-label-input-con m-r-5"  ref={node => this.leastP = node}>
                                <input className={(this.state.leastPEditable)? editableInputClass: nEditableInputClass} type="number" name='leastPValue' maxLength={5}
                                       onChange={this.handleChange} value={this.state.leastPValue} onKeyDown={this.handleOkEdit} ref={node => this.leastPInput=node}/>
                                <div className={(this.state.leastPEditable)? editableBtnClass: nEditableBtnClass} onClick={this.handleOkEdit}/>
                            </div>

                        </div>

                    </div>



                    <table className='t-align-c m-t-50 bill-table width-full'>
                        <thead>
                            <tr>
                                <th>شماره</th>
                                <th>تاریخ ایجاد</th>
                                <th>آخرین مهلت پرداخت</th>
                                <th>وضعیت</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>6652</td>
                                <td>1398-03-02</td>
                                <td>1398-06-02</td>
                                <td>پرداخت نشده</td>
                                <td>
                                    <div className="button" style={{marginBottom: '0'}} onClick={this.handlePay}>
                                        پرداخت
                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td>6652</td>
                                <td>1398-03-02</td>
                                <td>1398-06-02</td>
                                <td>پرداخت نشده</td>
                                <td>
                                    <button className="button" style={{marginBottom: '0'}}  onClick={this.handlePay}>
                                        پرداخت
                                    </button>
                                </td>
                            </tr>

                        </tbody>
                    </table>


                </div>

            </div>
        )
    }
}

const mapStateToProps = state => ({
    // managementDetails: state.data.managementDetails,

    location: state.data.location,
    courier: state.data.courier,
    leastPrice: state.data.leastPrice,
    maxDistance: state.data.maxDistance,
    dfo: state.data.dfo,
    bills: state.data.bills,

    showModal: state.data.showModal
})

export default connect(mapStateToProps, {fetchManagement, handleModal, updateManagement, handleStatusCode, safeRequest})(Management)
