import React, {Component} from 'react'

import food from '../../assets/icons/food.png'
import {connect} from "react-redux"
import {fetchActiveOrders, handleStatusCode, safeRequest} from "../../store/mainActions"
import {consts, log, postRequest, URLS} from "../../config/config"
import ActiveOrder from "../items/ActiveOrder"


// these objects final value won't change even when component unmounts and mounts again :)
let requestInProgress = false

class ActiveOrders extends Component {

    componentDidMount() {
        this.props.fetchActiveOrders()
    }

    render() {

        const activeOrders = this.props.activeOrders.map(order => <ActiveOrder key={order._id} order={order}/> )

        return (
            <div className="container-main">

                <div className="row dir-rtl">
                    {activeOrders}
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    activeOrders: state.data.activeOrders,
    activeOrdersListVersion: state.data.activeOrdersListVersion,
})

export default connect(mapStateToProps, {fetchActiveOrders, handleStatusCode, safeRequest})(ActiveOrders)
