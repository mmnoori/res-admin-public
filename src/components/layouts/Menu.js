import React, {Component} from 'react'
import {connect} from 'react-redux'

import picture from "../../assets/icons/picture.svg"

import {fetchFoods, handleModal, updateFood, handleSecondModal, handleStatusCode, safeRequest} from "../../store/mainActions"
import FoodSortbar from '../Sortbar/MenuSortbar'
import {consts, imagesAddress, log, postRequest, URLS} from "../../config/config"


class Menu extends Component {

    constructor() {
        super()

        this.fetchFoodsIfNeeded = this.fetchFoodsIfNeeded.bind(this)
    }

    componentDidMount(){
        this.fetchFoodsIfNeeded()
    }

    fetchFoodsIfNeeded() {

        if (this.props.foods.length >= 1 ) { // if was not first fetch

            this.props.safeRequest(URLS.ROUTE_LIST_VERSIONS, {}, (res) => {

                if (res.flv === this.props.foodListVersion) log('foodListVersion was NOT changed')

                else {
                    this.props.fetchFoods(this.props.foodStatus, this.props.foodLabel)
                    log('foodListVersion was changed')
                }
            }, this.props.handleStatusCode)

        } else {
            this.props.fetchFoods(this.props.foodStatus, this.props.foodLabel)
        }
    }

    updateFoodSuggested(food) {

        food.suggested = !food.suggested
        this.props.updateFood(food, null, this.props.foodStatus, this.props.foodLabel)
    }

    updateFoodAvailable(food) {

        food.available = !food.available
        this.props.updateFood(food, null, this.props.foodStatus, this.props.foodLabel)
    }


    render() {

        const foods = this.props.foods.map(food => (
            <div className='food-container' key={food._id}>
                <div key={food._id} className={(food.available)? 'food-layout': 'food-layout unavailable'}>

                    <img className="food-icon" src={(food.imgSrc)? imagesAddress + food.imgSrc: picture}/>

                    <div className="food-detail width-full">

                        <div className="food-row">

                            <div className="food-order-text input">{food.name}</div>

                            <div className="food-buttons" >

                                <div className="food-btn" onClick={() => this.updateFoodSuggested(food)}>
                                    <span className={(food.suggested)? 'fa fa-star star-margin yellow': 'fa fa-star star-margin'}/>ویژه
                                </div>

                                <div className="food-btn" onClick={() => this.updateFoodAvailable(food)}>
                                    <div className={(food.available)? 'food-btn-icon circle green': 'food-btn-icon circle red'}/>
                                    {(food.available)? consts.AVAILABLE: consts.NOT_AVAILABLE}
                                </div>

                                <div className="food-btn" onClick={() => this.props.handleModal(consts.MODAL_FOOD, null, null, {food})}>ویرایش</div>

                                <div className="food-btn" onClick={() => this.props.handleConfirmModal(consts.MODAL_CONFIRM, {food})}>حذف</div>

                            </div>

                        </div>

                        <div className="food-row" style={{marginBottom: '0'}}>
                            <div className="food-order-text p-l-20">دسته: {food.label}</div>
                            <div className="food-order-text p-l-20">قیمت: {food.price} تومان</div>
                            <div className="food-order-text p-l-20">تخفیف: {food.discount}%</div>
                        </div>

                        <hr/>

                        <div className="food-row" style={{marginBottom: '0'}}>
                            <div className="food-order-text ">توضیحات:</div>
                            <div className="text-sm-right food-order-text p-r-5">
                                {food.des}
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        ))


        return (
            <div className="container-main">

                <div className='dir-rtl'>

                    <FoodSortbar/>

                    <div className="row">

                        {foods}

                    </div>

                </div>

            </div>
        )
    }
}

const mapStateToProps = state => ({
    showModal: state.data.showModal,
    foods: state.data.foods,
    foodListVersion: state.data.foodListVersion,
    foodStatus: state.data.foodStatus,
    foodLabel: state.data.foodLabel
})

export default connect(mapStateToProps, {handleModal, fetchFoods, updateFood, handleConfirmModal: handleSecondModal, handleStatusCode, safeRequest})(Menu)
