import React, {Component} from 'react';
import {connect} from 'react-redux';

import {fetchOrders, handleModal, handleStatusCode, safeRequest, handleReqProcess} from "../../store/mainActions";

import Sortbar from '../Sortbar/OrderSortbar';
import food from "../../assets/icons/food.png";
import avatar from "../../assets/icons/avatar-01.png";
import location from "../../assets/icons/location.png";
import price from "../../assets/icons/price.png";
import payable from "../../assets/icons/payable.png";
import {log, postRequest, URLS, consts, getFormattedFoods} from "../../config/config";
import phone from "../../assets/icons/phone.png";



class Orders extends Component {

    constructor() {
        super();

        this.handleScroller = this.handleScroller.bind(this);
        this.fetchOrders = this.fetchOrders.bind(this);
    }

    componentDidMount() {
        this.fetchOrders(0, this.props.orderSort, this.props.orderStatus);
    }

    handleScroller(e) {

        // باقی مانده گیری تعداد آیتم ها بر chunk count روش خوبی هست ولی ممکنه کل تعداد یه لیست ضریبی از chunk count باشه پس نمیتونیم همیشه برای تشخیص انتهای لیست ازش استفاده کنیم
        // if request already sent or there are no more results
        if (this.props.reqInProcess === true || this.props.orderEnd === true) return

        if (e.target.scrollHeight - e.target.scrollTop <= 1000) {

            this.props.handleReqProcess(true);
            this.fetchOrders(this.props.orders.length);
        }
    }

    fetchOrders(skipCount) {
        this.props.fetchOrders(skipCount, this.props.orderSort, this.props.orderStatus, this.props.orderDate, this.props.orderSearchText, consts.ORDERS_CHUNK_COUNT);
    }


    render() {

        let OrderBadge = (orderState) => {

            if (orderState.props === consts.REJECTED)
                return <span className="order-badge danger flt-l ">{orderState.props}</span>;
            else
                return <span className="order-badge success flt-l ">{orderState.props}</span>
        };

        const orderItems = this.props.orders.map((order, index) => (

            <div className="order-con-small" key={order._id}>
                <div className="order-layout">

                    <div className="item-head t-align-r">

                        <span className={`order-badge flt-l ${(order.state === consts.REJECTED)? 'red': 'green'}`}>{order.state}</span>
                        <strong className="card-title">{order.code}</strong>

                    </div>

                    <div className="item-body">
                        <div className="food-order-text">
                            <img className="food-order-icon" src={food}/>
                            {getFormattedFoods(order.foods)}
                        </div>

                        <div className="food-order-text clickable m-t-5"
                             onClick={() => this.props.handleModal(consts.MODAL_CUSTOMER, null, [], {phone: order.owner.phone, name: order.owner.name})}>
                            <img className="food-order-icon" src={avatar}/>
                            {order.owner.name}
                        </div>

                        <div className="food-order-text">
                            <img className="food-order-icon" src={phone}/>
                            {order.phone} | {order.telePhone}
                        </div>


                        <div className="food-order-text clickable m-t-5"
                             onClick={() => this.props.handleModal(consts.MODAL_SIMPLE_MAP, null, [], {location: order.location})}>
                            <img className="food-order-icon" src={location}/>
                            {order.address}
                        </div>

                        <div className="food-order-text m-t-5">
                            <img className="food-order-icon" src={price}/>
                            قیمت کل : {order.price} تومان
                        </div>

                        <div className="food-order-text">
                            <img className="food-order-icon" src={payable}/>

                            {(order.method === consts.ONLINE)?
                                <span>شسیشسی</span>:
                                `قابل پرداخت : ${order.payable} تومان`
                            }

                        </div>

                        <hr className='m-t-a'/>

                        <strong className="">
                            <span className="flt-l mt-1 food-order-text more-button" onClick={() => this.props.handleModal(consts.MODAL_ORDER, null, [], {order})}>بیشتر</span>
                            <span className="flt-r mt-1 food-order-text" style={{direction: 'ltr'}}>{order.created}</span>
                        </strong>

                    </div>
                </div>
            </div>
        ));


        return (
            <div className="container-main" ref={node => this.container=node} onScroll={this.handleScroller}>

                <div className='dir-rtl'>

                    <Sortbar />

                    <div className="row width-full flt-r">

                        {orderItems}
                    </div>


                    {/* حواست باشه برای loader یه طوری استایل بدی که Scroll height تغییر نکنه در غیر اینصورت اگه کاربر سریع به ته لیست scroll کنه و height هم همون موقع زیاد بشه تو یه scroll event بی نهایت می افتیم */}

                    {(!this.props.orderEnd)?
                        <div className='width-full flt-r m-t-15 m-b-15'>
                            <div className='loader medium'/>
                        </div>
                        :
                        ''
                    }

                </div>

            </div>
        )
    }
}

const mapStateToProps = state => ({
    orders: state.data.orders,
    orderSort: state.data.orderSort,
    orderStatus: state.data.orderStatus,
    orderDate: state.data.orderDate,
    orderSearchText: state.data.orderSearchText,
    orderEnd: state.data.orderEnd,

    reqInProcess: state.data.reqInProcess,
});


export default connect(mapStateToProps, {fetchOrders, handleModal, handleStatusCode, safeRequest, handleReqProcess})(Orders);
