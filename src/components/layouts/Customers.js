import React, {Component} from 'react'
import {connect} from 'react-redux'

import {fetchCustomers, handleModal, closeModal, handleStatusCode, safeRequest, handleReqProcess} from "../../store/mainActions"
import {consts, log, monthToString, URLS} from "../../config/config"
import CustomerSortbar from '../Sortbar/CustomerSortbar'



class Customers extends Component {

    constructor() {
        super()

        this.handleScroller = this.handleScroller.bind(this)

        this.state = {
            checkAll: false,
            checkedItems: new Map()
        }

        this.checkAll = this.checkAll.bind(this)
        this.handleCheck = this.handleCheck.bind(this)
        this.fetchCustomers = this.fetchCustomers.bind(this)
    }

    componentDidMount() {
        this.fetchCustomers(0)
    }

    handleScroller(e) {
        // if request already sent or there are no more results
        if (this.props.reqInProcess === true || this.props.customerEnd === true) return

        if (e.target.scrollHeight - e.target.scrollTop <= 1000) {

            this.props.handleReqProcess(true)
            this.fetchCustomers(this.props.customers.length)
        }
    }

    fetchCustomers(skipCount) {
        this.props.fetchCustomers(skipCount, this.props.customerSort, this.props.customerStatus, this.props.customerSearchText, consts.CUSTOMERS_CHUNK_COUNT)
    }

    handleCheck(e) {

        const item = e.target.value
        const isChecked = e.target.checked

        if (this.state.checkAll) return // dont interrupt if checkAll is active

        if (isChecked === true) // check
            this.setState(prevState => ({ checkedItems: prevState.checkedItems.set(item, isChecked) }))
        else { // uncheck
            let checkedItems = this.state.checkedItems
            checkedItems.delete(item)
            this.setState(({ checkedItems }))
        }

    }

    checkAll(event) {

        // clearing checkItems map and checking/unchecking all
        this.setState({
            checkAll: !this.state.checkAll,
            checkedItems: new Map() // wiping manually checkedItems
        })
    }


    render() {

        const customerItems = this.props.customers.map(customer => (

            <tr key={customer.code} onClick={() =>  this.props.handleModal(consts.MODAL_CUSTOMER, null, [], {phone: customer.phone, name: customer.name, })}>

                <td>
                    <input className="customer-checkbox" type="checkbox" value={customer.code}
                           onClick={(e) => e.stopPropagation()} onChange={this.handleCheck}
                        // checked={true}
                           checked={(this.state.checkedItems.get(customer.code) === undefined)? this.state.checkAll: (this.state.checkedItems.get(customer.code))}
                    />
                </td>

                <td>{customer.code}</td>

                <td className={(this.props.customerStatus===consts.BANNED)? "denied":"process"}>{customer.name}</td>
                <td>{customer.phone}</td>

                {(customer.birth)? <td className=''>{`${Number(customer.birth.day)} ${monthToString(customer.birth.month)}`}</td> : <td/>}

                <td>{customer.ordersCount}</td>
                <td>{customer.invitedsCount}</td>
                <td>{customer.created}</td>

            </tr>
        ))

        return (
            <div className="container-main" onScroll={this.handleScroller}>

                <div className='dir-rtl'>

                    <CustomerSortbar openModalCondition={this.state.checkedItems.size !== 0 || this.state.checkAll}
                                     customers={(this.state.checkAll)? consts.ALL: this.state.checkedItems}/>

                    <table className="table-customer m-b-40 flt-r width-full">

                        <thead>
                        <tr className='t-align-c'>

                            <th>
                                <i className={`fas fa-check check ${(this.state.checkAll)? 'active': '' }`} onClick={this.checkAll}/>
                            </th>
                            <th>کد</th>
                            <th>نام</th>
                            <th>شماره همراه</th>
                            <th>تولد</th>
                            <th>تعداد سفارش</th>
                            <th>تعداد دعوت</th>
                            <th>تاریخ عضویت</th>

                        </tr>
                        </thead>

                        <tbody className='t-align-c'>

                        {customerItems}

                        </tbody>

                    </table>

                    {(!this.props.customerEnd)?
                        <div className='width-full flt-r m-t-15 m-b-15'>
                            <div className='loader medium'/>
                        </div>
                        :
                        ''
                    }
                </div>
            </div>
        )
    }
}


const mapStateToProps = state => ({
    customers: state.data.customers,
    customerSort: state.data.customerSort,
    customerStatus: state.data.customerStatus,
    customerSearchText: state.data.customerSearchText,
    customerEnd: state.data.customerEnd,

    reqInProcess: state.data.reqInProcess,
})

export default connect(mapStateToProps, {fetchCustomers, handleModal, closeModal, handleStatusCode, safeRequest, handleReqProcess})(Customers)
