import React, {Component} from 'react'
import {connect} from 'react-redux'

import {fetchEvents, handleModal, handleSecondModal, showToast, handleStatusCode, safeRequest, handleReqProcess} from "../../store/mainActions"
import {consts, imagesAddress, log, URLS} from "../../config/config"
import picture from "../../assets/icons/picture.svg"

class Events extends Component {

    constructor(props) {
        super(props)

        this.newEvent = this.newEvent.bind(this)
        this.handleScroller = this.handleScroller.bind(this)
        this.fetchEvents = this.fetchEvents.bind(this)

    }

    componentDidMount() {
        this.fetchEvents(0)
    }

    handleScroller(e) {
        // if request already sent or there are no more results
        if (this.props.reqInProcess === true || this.props.eventEnd === true) return

        if (e.target.scrollHeight - e.target.scrollTop <= 1000) {

            this.props.handleReqProcess(true)
            this.fetchEvents(this.props.events.length)
        }
    }

    fetchEvents(skipCount) {
        this.props.fetchEvents(skipCount, consts.EVENTS_CHUNK_COUNT)
    }

    htmlDecode(input){
        var e = document.createElement('div')
        e.innerHTML = input
        return e.childNodes.length === 0 ? "" : e.childNodes[0].nodeValue
    }

    newEvent() {
        this.props.handleModal(consts.MODAL_EVENT)
    }


    render() {

        const events = this.props.events.map(event => (
            <div className='food-container' key={event._id}>
                <div key={event._id} className='food-layout'>

                    <img className="food-icon" src={(event.imgSrc)? imagesAddress + event.imgSrc: picture}/>

                    <div className="food-detail width-full">

                        <div className="food-row">

                            <div className="food-order-text input">{event.title}</div>

                            <div className="food-buttons" >

                                <div className={`food-btn ${(event.notification)? 'blue': ''}`}
                                     onClick={() => this.props.showToast('امکان تغییر اعلان برای خبر ارسال شده وجود ندارد')}>
                                    <span className='far fa-bell star-margin'/>نوتیفیکیشن
                                </div>

                                <div className="food-btn" onClick={() => this.props.handleModal(consts.MODAL_EVENT, null, null, {event})}>ویرایش</div>

                                <div className="food-btn" onClick={() => this.props.handleConfirmModal(consts.MODAL_CONFIRM, {event})}>حذف</div>

                            </div>

                        </div>

                        <div className="food-row" style={{marginBottom: '0'}}>
                            <div className="text-sm-right food-order-text p-r-5">
                                <div className='text' dangerouslySetInnerHTML={{ __html: event.content}} />
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        ))

        return (
            <div className="container-main" onScroll={this.handleScroller}>

                <div className='dir-rtl'>

                    <div className="row m-b-25">

                        <div className="button m-r-45" onClick={this.newEvent}>
                            خبر جدید
                        </div>

                    </div>


                    <div className="row">

                        {events}

                    </div>



                    {(!this.props.eventEnd)?
                        <div className='width-full flt-r m-t-15 m-b-15'>
                            <div className='loader medium'/>
                        </div>
                        :
                        ''
                    }
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    events: state.data.events,
    eventEnd: state.data.eventEnd,

    reqInProcess: state.data.reqInProcess
})

export default connect(mapStateToProps, { handleModal, fetchEvents, handleConfirmModal: handleSecondModal,
    handleReqProcess, showToast, handleStatusCode, safeRequest})(Events)
