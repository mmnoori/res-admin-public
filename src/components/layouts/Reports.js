import React, {Component} from 'react';
import {connect} from "react-redux";
import {closeModal, fetchReports, handleModal, handleReqProcess, handleStatusCode, safeRequest} from "../../store/mainActions";
import {consts, monthToString} from "../../config/config";
import Stars from "../utils/Stars";

class Reports extends Component {

    constructor() {
        super();


        this.state = {
        };

        this.handleScroller = this.handleScroller.bind(this);
        this.fetchReports = this.fetchReports.bind(this);
    }

    componentDidMount() {
        this.fetchReports(0);
    }

    handleScroller(e) {
        // if request already sent or there are no more results
        if (this.props.reqInProcess === true || this.props.reportEnd === true) return;

        if (e.target.scrollHeight - e.target.scrollTop <= 1000) {

            this.props.handleReqProcess(true);
            this.fetchReports(this.props.reports.length);
        }
    }

    fetchReports(skipCount) {
        this.props.fetchReports(skipCount, consts.REPORTS_CHUNK_COUNT);
    }


    render() {

        return (
            <div className="container-main" onScroll={this.handleScroller}>

                <div className='dir-rtl row'>

                    {this.props.reports.map((report) =>
                        <>

                            <div className='order-con'>
                                <div className='order-layout'>

                                    <div className='item-head dis-flex'>

                                        <span className='order-badge gray m-r-0' style={{fontSize: '13px'}}>{report.title}</span>

                                    </div>

                                    <div className='item-body'>

                                        <div className='food-order-text'>{report.desc}</div>

                                        <hr className='m-t-a'/>

                                        <div className='dir-ltr'>{report.created}</div>

                                    </div>

                                </div>
                            </div>


                        </>
                        )
                    }


                    {(!this.props.reportEnd)?
                        <div className='width-full flt-r m-t-15 m-b-15'>
                            <div className='loader medium'/>
                        </div>
                        :
                        ''
                    }
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    reports: state.data.reports,
    reportEnd: state.data.reportEnd,

    reqInProcess: state.data.reqInProcess,
});

export default connect(mapStateToProps, {fetchReports, handleModal, closeModal, handleStatusCode, safeRequest, handleReqProcess})(Reports);
