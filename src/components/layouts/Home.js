import React, {Component} from 'react'
import { Bar } from 'react-chartjs-2'

import {baseAddress, consts, convertNumbersToEnglish, getFormattedPrice, log, postRequest, URLS} from '../../config/config'

import close from "../../assets/icons/close.png"
import {connect} from "react-redux"
import {handleReqProcess, handleStatusCode, safeRequest} from "../../store/mainActions"


class Home extends Component {

    constructor(props) {
        super(props)

        let year = new Date().toLocaleDateString('fa-IR',{year: 'numeric'})
        const month = new Date().toLocaleDateString('fa-IR',{month: "long" })

        // we have problem with arabic digits in server side
        year = convertNumbersToEnglish(year)


        this.state = {
            monthDropdown: false,
            yearDropdown: false,
            year,
            month,

            labels: [],
            views: [],
            customers: {},
            sale: [],
            orders: [],

            sum: {
                views: 0,
                orders: 0,
                customers: 0,
                sale: 0,
            }
        }

        this.handleDropdown = this.handleDropdown.bind(this)
        this.closeDropdown = this.closeDropdown.bind(this)
        this.removeDate = this.removeDate.bind(this)
        this.handleYear = this.handleYear.bind(this)
        this.handleMonth = this.handleMonth.bind(this)
        this.sendReq = this.sendReq.bind(this)

    }

    componentDidMount() {
        document.addEventListener('mousedown', this.closeDropdown)

        this.sendReq(this.state.year, this.state.month)
    }
    componentWillUnmount() {
        document.removeEventListener('mousedown', this.closeDropdown)
    }

    closeDropdown(event) {

        if (!this.monthBtn.contains(event.target) && !this.yearBtn.contains(event.target)) {

            this.setState({
                dayDropdown: false,
                monthDropdown: false,
                yearDropdown: false,
            })
        }
    }


    handleDropdown(event) {

        if (this.monthBtn.contains(event.target)) {

            this.setState({
                monthDropdown: !this.state.monthDropdown,
                yearDropdown: false,
            })

        } else if (this.yearBtn.contains(event.target)) {

            this.setState({
                monthDropdown: false,
                yearDropdown: !this.state.yearDropdown,
            })
        }

    }

    removeDate(e) {

        // prevent from from executing other click listeners
        e.stopPropagation()

        if (this.yearBtn.contains(e.target)) {
            this.setState({ yearDropdown: false, year: consts.YEAR})

            this.sendReq()

        } else if (this.monthBtn.contains(e.target)) {
            this.setState({ monthDropdown: false, month: consts.MONTH})

            this.sendReq(this.state.year)
        }

    }

    handleYear(e) {
        if (this.props.reqInProcess) return

        const year = e.currentTarget.textContent

        this.setState({year, month: consts.MONTH})

        this.sendReq(year)
    }

    handleMonth(e) {
        if (this.props.reqInProcess) return

        const month = e.currentTarget.textContent

        this.setState({month})

        this.sendReq(this.state.year, month)
    }

    sendReq(year, month) {

        this.props.handleReqProcess(true)

        this.props.safeRequest(URLS.ROUTE_GET_STATICS, {month, year}, (data, statusCode) => {

            log(data)

            this.setState({ labels: data.labels, views: data.views, customers: data.customers, sale: data.sale, orders: data.orders,
            sum: {...this.state.sum, ...data.sum} })

            this.props.handleReqProcess(false)

        }, this.props.handleStatusCode)
    }


    render() {

        let btnStyles = {
            todayBtn : {},
            overallBtn: {}
        }


        const options = {
            maintainAspectRatio: false,
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true,
                        min: 0,
                        callback: function (value) { if (Number.isInteger(value))  return getFormattedPrice(value)  },
                        // stepSize: 1
                    }
                }]
            },
            legend: {
                labels: {
                    fontColor: 'black',
                    fontFamily: 'Arial',
                    fontSize: 18
                }
            }
        }

        const data = { labels: this.state.labels, }

        const isYearSelected = this.state.year === consts.YEAR
        const isMonthSelected = this.state.month === consts.MONTH

        const labels = this.state.labels

        // log(this.state.views)

        return (
            <div className="container-main">

                <div className="dir-rtl">


                    <div className="dis-flex">


                        <div className="pick-date width-140" onClick={this.handleDropdown} ref={node => this.yearBtn = node}>

                            <span className={`close-btn ${isYearSelected? 'dis-none': ''}`}
                                  onClick={this.removeDate} ref={node => this.removeDateBtn = node}/>

                            {this.state.year}
                            <span className={`arrow-down dis-inline-b ${isYearSelected? 'dis-none': ''}`}/>

                            {(this.state.yearDropdown)?
                                <ul className='date-dropdown'>
                                    <li onClick={this.handleYear}>1401</li>
                                    <li onClick={this.handleYear}>1402</li>
                                </ul>
                                :''
                            }

                        </div>


                        <div className={`pick-date width-140 m-r-10 ${isYearSelected? 'dis-none': ''}`}
                             onClick={this.handleDropdown} ref={node => this.monthBtn = node}>

                            <span className={`close-btn ${isMonthSelected? 'dis-none': ''}`}
                                 onClick={this.removeDate} ref={node => this.removeDateBtn = node}/>

                            {this.state.month}
                            <span className={`arrow-down dis-inline-b ${isMonthSelected? 'dis-none': ''}`}/>

                            {(this.state.monthDropdown)?
                                <ul className='date-dropdown'>
                                    <li onClick={this.handleMonth}>{consts.FARVARDIN}</li>
                                    <li onClick={this.handleMonth}>{consts.ORDIBEHESHT}</li>
                                    <li onClick={this.handleMonth}>{consts.KHORDAD}</li>
                                    <li onClick={this.handleMonth}>{consts.TIR}</li>
                                    <li onClick={this.handleMonth}>{consts.MORDAD}</li>
                                    <li onClick={this.handleMonth}>{consts.SHAHRIVAR}</li>
                                    <li onClick={this.handleMonth}>{consts.MEHR}</li>
                                    <li onClick={this.handleMonth}>{consts.ABAN}</li>
                                    <li onClick={this.handleMonth}>{consts.AZAR}</li>
                                    <li onClick={this.handleMonth}>{consts.DEY}</li>
                                    <li onClick={this.handleMonth}>{consts.BAHMAN}</li>
                                    <li onClick={this.handleMonth}>{consts.ESFAND}</li>
                                </ul>
                                    :''
                            }

                        </div>

                    </div>




                    <div className="row m-t-25 m-b-40">

                        <div className="overview-item-con" >
                            <div className="overview-item overview-item--c1">
                                <div className="">
                                    <div className="overview-box clearfix">
                                        <div className="icon">
                                        </div>
                                        <div className="text">
                                            <h2>{getFormattedPrice(this.state.sum.customers)}</h2>
                                            <span>کاربران جدید</span>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div className="overview-item-con">
                            <div className="overview-item overview-item--c2" >
                                <div className="">
                                    <div className="overview-box clearfix">
                                        <div className="icon">
                                        </div>
                                        <div className="text">
                                            <h2>{getFormattedPrice(this.state.sum.orders)}</h2>
                                            <span>تعداد سفارش ها</span>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div className="overview-item-con" >
                            <div className="overview-item overview-item--c4">
                                <div className="">
                                    <div className="overview-box clearfix">
                                        <div className="icon">
                                        </div>
                                        <div className="text">
                                            <h2>{getFormattedPrice(this.state.sum.views)}</h2>
                                            <span>تعداد بازدید</span>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div className="overview-item-con" >
                            <div className="overview-item overview-item--c3">
                                <div className="">
                                    <div className="overview-box clearfix">
                                        <div className="icon">
                                            <i className="zmdi zmdi-calendar-note"/>
                                        </div>
                                        <div className="text">
                                            <h2>{getFormattedPrice(this.state.sum.sale)}</h2>
                                            <span>میزان فروش (تومان)</span>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>





                    <div className="d-flex">


                        {(this.props.reqInProcess)?
                            ''
                            :
                            <>

                                <div className='chart-con'>

                                    <Bar
                                        data={{
                                            labels,
                                            datasets: [{
                                                data: this.state.sale,
                                                label: 'میزان فروش',
                                                backgroundColor: '#ff5800',
                                                borderColor: '#b64200',
                                                borderWidth: 1,
                                                hoverBackgroundColor: '#ff9c51',
                                                hoverBorderColor: '#b64200',
                                            }]
                                        }}
                                        // width={50}
                                        // height={50}
                                        options={options} />

                                </div>



                                <div className='chart-con' >

                                    <Bar
                                        data={{
                                            labels,
                                            datasets: [{
                                                data: this.state.orders,
                                                label: 'تعداد سفارش',
                                                backgroundColor: '#00c143',
                                                borderColor: '#00802d',
                                                borderWidth: 1,
                                                hoverBackgroundColor: '#00d84c',
                                                hoverBorderColor: '#00802d',
                                            }]
                                        }}
                                        width={100}
                                        height={50}
                                        options={options} />

                                </div>

                                <div className='chart-con'  >

                                    <Bar
                                        data={{
                                            labels,
                                            datasets: [{
                                                data: this.state.customers,
                                                label: 'کاربران جدید',
                                                backgroundColor: '#9198e5',
                                                borderColor: '#6b73b1',
                                                borderWidth: 1,
                                                hoverBackgroundColor: '#a3abff',
                                                hoverBorderColor: '#6b73b1',
                                            }]
                                        }}
                                        width={100}
                                        height={50}
                                        options={options} />

                                </div>

                                <div className='chart-con'  >

                                    <Bar
                                        data={{
                                            labels,
                                            datasets: [{
                                                data: this.state.views,
                                                label: 'بازدید',
                                                backgroundColor: '#bed75a',
                                                    borderColor: '#a4ab47',
                                                    borderWidth: 1,
                                                    hoverBackgroundColor: '#cae879',
                                                    hoverBorderColor: '#a4ab47',
                                            }]
                                        }}
                                        width={100}
                                        height={50}
                                        options={options} />

                                </div>

                            </>
                        }


                    </div>




                    <div className="t-align-c">
                        <div className="copyright" style={{fontSize: '9px'}}>
                            <p>پنل ادمین مدیریت سفارش های آنلاین</p>
                        </div>
                    </div>

                </div>
            </div>
        )
    }
}

const mapStateToProps = state => ({

    reqInProcess: state.data.reqInProcess,
})


export default connect(mapStateToProps, {handleStatusCode, safeRequest, handleReqProcess})(Home)
