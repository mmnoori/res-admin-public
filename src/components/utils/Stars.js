import React, {Component} from 'react';
import food from "../../assets/icons/food.png";
import motor from "../../assets/icons/motor.png";
import comment from "../../assets/icons/comment.png";

/* gets order.score as props */
class Stars extends Component {

    render() {

        const order = this.props.order;

        let courierStyle = [];
        let foodStyle = [];
        let appStyle = [];

        let stars = (styleArray) => {
            return (
                <div className="dir-ltr">
                    <span className={'fa fa-star p-t-4' + ' ' + styleArray[1]}/>
                    <span className={'fa fa-star' + ' ' + styleArray[2]}/>
                    <span className={'fa fa-star' + ' ' + styleArray[3]}/>
                    <span className={'fa fa-star' + ' ' + styleArray[4]}/>
                    <span className={'fa fa-star' + ' ' + styleArray[5]}/>
                </div>
            )
        }

        // if order was scored
        if (order.score) {

            // if order is scored, it has at least one star
            courierStyle[1] = " yellow";
            foodStyle[1] = " yellow";
            appStyle[1] = " yellow";

            for (let i = 2; i <= 5; i++) {

                if (i <= order.score.food) foodStyle[i] = "yellow";
                else foodStyle[i] = "";

                if (i <= order.score.courier) courierStyle[i] = "yellow";
                else courierStyle[i] = "";

                if (i <= order.score.app) appStyle[i] = "yellow";
                else appStyle[i] = "";

                if (i === 5) {
                    courierStyle[i] += ' p-r-5';
                    foodStyle[i] += ' p-r-5';
                    appStyle[i] += ' p-r-5';
                }
            }

        } else  // if order was NOT scored
            stars = () => <span className="food-order-text p-r-5">ثبت نشده</span>



        return (
            <>

                <div className="dis-flex">
                    <div className="food-order-text width-100">
                        <img className="food-order-icon" src={food}/>
                        امتیاز غذا :
                    </div>
                    {stars(foodStyle)}
                </div>

                <div className="dis-flex">
                    <div className="food-order-text width-100">
                        <img className="food-order-icon" src={motor}/>
                        امتیاز پیک :
                    </div>
                    {stars(courierStyle)}
                </div>

                <div className="dis-flex">
                    <div className="food-order-text width-100">
                        <span className="food-order-icon fab fa-internet-explorer" />
                        امتیاز سایت :
                    </div>
                    {stars(appStyle)}
                </div>

                <div className="text-sm-right food-order-text">
                    <img className="food-order-icon" src={comment}/>
                    نظر : {(order.score)? order.score.comment: ''}
                </div>

            </>
        )

    }
}

export default Stars;
