import React, { Component } from 'react'
import { Redirect } from 'react-router-dom'

import {URLS, log, postRequest} from '../../config/config'

export default function withAuth(ComponentToProtect) {

    return class extends Component {

        constructor() {
            super()

            this.state = {
                loading: true,
                redirect: false,
            }
        }

        componentDidMount() {

            postRequest(URLS.ROUTE_CHECK_TOKEN, {}, (data, statusCode) => {
                if (statusCode === 200) {
                    this.setState({ loading: false })
                    // this.props.history.push('/');
                } else
                    this.setState({ loading: false, redirect: true })
            })

        }


        render() {
            const { loading, redirect } = this.state

            if (loading) return null

            if (redirect) return <Redirect to="/login" />

            return (
              <>
                <ComponentToProtect {...this.props} />
              </>
            )
        }
    }
}
