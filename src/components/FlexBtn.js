import React, {Component} from 'react';
import { connect } from 'react-redux';

import {closeModal,  showToast} from "../store/mainActions";
import { consts} from '../config/config'

/*
* gets handleSubmit() and processable: bool, className: string as props
* */
class FlexBtn extends Component {
    render() {

        let submitClass = 'btn-save-food ' + this.props.className;
        let btnText = consts.SAVE;

        if (this.props.show === consts.MODAL_FOOD_GROUP_DISCOUNT)
            submitClass += 'm-b-10';
        else if (this.props.show === consts.MODAL_MEALS)
            submitClass += 'm-b-10';
        else if (this.props.show === consts.MODAL_ADDRESSES) {
            submitClass = 'width-180 a-self-c';
            btnText = 'آدرس جدید'
        }
        else if (this.props.show === consts.MODAL_ADDRESS) {
            // submitClass = 'width-180 a-self-c';
            btnText = consts.NEXT
        }



        if (this.props.title) // if there is any title sent in as props
            btnText = this.props.title;

        return (
            <>
                {/* using bool processable to define if button should not turn to a loader */}
                {(this.props.reqInProcess && !this.props.processable)?
                    <button className={submitClass} type="button"><div className='loader'/></button>:
                    <button className={submitClass} type="button"  onClick={this.props.handleSubmit}>{btnText}</button>
                }
             </>
        );
    }
}

const mapStateToProps = state => ({
    reqInProcess: state.data.reqInProcess,
    show: state.data.showModal,
});

export default connect(mapStateToProps, {closeModal, showToast})(FlexBtn);
