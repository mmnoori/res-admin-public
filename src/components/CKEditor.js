import React, { Component } from 'react'
import CKEditor from '@ckeditor/ckeditor5-react'
import ClassicEditor from '@ckeditor/ckeditor5-build-classic'

/*
* gets currentData and getData as props
* */
class MyCKEditor extends Component {

    render() {
        return (
            <CKEditor
                ref={node => this.editor=node}
                editor={ ClassicEditor }
                data={this.props.currentData}

                // برای لیست های ul , li باید هر جایی که قراره استفاده بشه، استایل های مشترک بدیم

                config={{
                    // removePlugins: [ 'Italic', 'Insert image', 'Image', 'blockQuote' ],
                    toolbar: [
                        'bold',
                        // '|',
                        'link',
                        // '|',
                        // 'bulletedList',
                        // '|',
                        // 'numberedList',
                        // '|',
                    ],
                    language: {
                        // The UI will be English.
                        ui: 'en',
                        // But the content will be edited in Arabic.
                        content: 'ar'
                    },
                } }
                onInit={ editor => {
                    // You can store the "editor" and use when it is needed.
                }}
                onChange={ ( event, editor ) => {
                    const data = editor.getData()
                    // console.log( { event, editor, data } )
                    this.props.getData(data)
                } }
                onBlur={ ( event, editor ) => {
                    // console.log( 'Blur.', editor )
                } }
                onFocus={ ( event, editor ) => {
                    // console.log( 'Focus.', editor )
                } }
            />
        )
    }
}

export default MyCKEditor
