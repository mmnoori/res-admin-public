import React, {Component} from 'react'
import { NavLink } from 'react-router-dom'
import {connect} from 'react-redux'

import { baseAddress, log } from '../config/config'

import plate1Svg from '../assets/icons/plate1.svg'
import plate2Svg from '../assets/icons/plate2.svg'
import homePng from '../assets/icons/home.png'
import activePng from '../assets/icons/active.png'
import historyPng from '../assets/icons/history.png'
import customerPng from '../assets/icons/customer.png'
import menuPng from '../assets/icons/menu.png'
import postPng from '../assets/icons/post.png'
import commentPng from '../assets/icons/comment.png'
import managePng from '../assets/icons/manage.png'
import reportPng from '../assets/icons/report.png'
import helpPng from '../assets/icons/help.png'

/*
* This component would be converted to header on mobile view
* */
class Sidebar extends Component {

    constructor(props) {
        super(props)

        this.state = {
            home: '',
            activeOrders: '',
            ordersHistory: '',
            customers: '',
            menu: '',
            reports: '',
            events: '',
            comments: '',
            management: '',
            help: '',
        }
    }

    componentWillReceiveProps(nextProps) {

        // resetting tags
        this.setState({
            home: '',
            activeOrders: '',
            ordersHistory: '',
            customers: '',
            menu: '',
            reports: '',
            events: '',
            comments: '',
            management: '',
            help: '',
        })

        switch (nextProps.route) {
            case '/':
                this.setState({home: 'active'})
                break

            case '/active-orders':
                this.setState({activeOrders: 'active'})
                break

            case '/orders':
                this.setState({ordersHistory: 'active'})
                break

            case '/customers':
                this.setState({customers: 'active'})
                break

            case '/menu':
                this.setState({menu: 'active'})
                break

            case '/reports':
                this.setState({reports: 'active'})
                break

            case '/events':
                this.setState({events: 'active'})
                break

            case '/comments':
                this.setState({comments: 'active'})
                break

            case '/management':
                this.setState({management: 'active'})
                break

            case '/help':
                this.setState({help: 'active'})
                break
        }
    }

    componentDidUpdate() {

    }

    componentDidMount() {

    }


    render() {

        return (
            <aside className="menu-sidebar d-lg-block">

                <div className="logo">
                    <img style={{height: '85%'}} src={plate1Svg}/>
                    <img style={{height: '90%'}} src={plate2Svg}/>
                </div>


                <div className='column height-full navbar-sidebar-con'>

                <div className="navbar-sidebar">

                    <ul className="navbar__list t-align-r" >

                        <li className={this.state.home}>
                            <NavLink to="/">
                                پیش خوان
                                <img className="sidebar-menu-icon" src={homePng}/>
                            </NavLink>
                        </li>
                        <li className={this.state.activeOrders}>
                            <NavLink to="/active-orders">
                                سفارش های فعال
                                <img className="sidebar-menu-icon" src={activePng}/>
                            </NavLink>
                        </li>
                        <li className={this.state.ordersHistory}>
                            <NavLink to="/orders">
                                تاریخچه سفارش ها
                                <img className="sidebar-menu-icon" src={historyPng}/>
                            </NavLink>
                        </li>
                        <li className={this.state.customers}>
                            <NavLink to="/customers">
                                مشتری ها
                                <img className="sidebar-menu-icon" src={customerPng}/>
                            </NavLink>
                        </li>
                        <li className={this.state.menu}>
                            <NavLink to="/menu">
                                منو
                                <img className="sidebar-menu-icon" src={menuPng}/>
                            </NavLink>
                        </li>
                        <li className={this.state.events}>
                            <NavLink to="/events">
                                اخبار و رویداد ها
                                <img className="sidebar-menu-icon" src={postPng}/>
                            </NavLink>
                        </li>
                        <li className={this.state.reports}>
                            <NavLink to="/reports">
                                گزارشات
                                <img className="sidebar-menu-icon" src={reportPng}/>
                            </NavLink>
                        </li>
                        <li className={this.state.comments}>
                            <NavLink to="/comments">
                                نظرات
                                <img className="sidebar-menu-icon" src={commentPng}/>
                            </NavLink>
                        </li>
                        <li className={this.state.management}>
                            <NavLink to="/management">
                                مدیریت
                                <img className="sidebar-menu-icon" src={managePng}/>
                            </NavLink>
                        </li>
                        <li className={this.state.help}>
                            <NavLink to="/help">
                                راهنما
                                <img className="sidebar-menu-icon" src={helpPng}/>
                            </NavLink>
                        </li>
                    </ul>
                </div>


                <div className="slide-date">
                    {new Date().toLocaleDateString('fa-IR', {year: 'numeric', month: "2-digit", day: "2-digit", weekday: 'long' })}
                </div>

                </div>


            </aside>
        )
    }
}



const mapStateToProps = state => ({
    route: state.data.currentRoute,
})

export default connect(mapStateToProps, {})(Sidebar)

/*
function add(x){
    return function(y){
        return x + y;
    };
}

var addTwo = add(2);

addTwo(4) === 6; // true
add(3)(4) === 7; // true
*/

