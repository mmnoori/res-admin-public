import React, {useState, useEffect} from "react"
import { useSelector, useDispatch } from 'react-redux'

import {handleOrder, handleSecondModal, handleModal} from '../../store/mainActions'

import {consts, getFormattedFoods, getFormattedPrice, log} from "../../config/config"
import food from "../../assets/icons/food.png"
import avatar from "../../assets/icons/avatar-01.png"
import phone from "../../assets/icons/phone.png"
import location from "../../assets/icons/location.png"
import price from "../../assets/icons/price.png"
import motor from "../../assets/icons/motor.png"
import discount from "../../assets/icons/discount.png"
import creditSpent from "../../assets/icons/credit-spent.png"
import payable from "../../assets/icons/payable.png"
import atm from "../../assets/icons/atm.png"

/*
* gets order as props
* */
const ActiveOrder = (props) => {

    const order = props.order

    const [loading, setLoading] = useState(false)

    useEffect(() => {
        setLoading(false)

    }, [order.state])



    const dispatch = useDispatch()


    function openMap() {
        dispatch(handleModal(consts.MODAL_SIMPLE_MAP, null, [], {location: order.location}))
    }

    function buttonClick(orderId, prevState) {

        let newState

        if (prevState === consts.COMMITTED) newState = consts.ACCEPTED
        else if (prevState === consts.ACCEPTED) newState = consts.SENT
        else if (prevState === consts.SENT) newState = consts.DELIVERED

        dispatch(handleOrder(orderId, newState, order.owner.phone))
        setLoading(true)
    }

    function handleCancel() {
        dispatch(handleSecondModal(consts.MODAL_CONFIRM, {activeOrder: order}))
    }




    let btnTxt = ''
    if (order.state === consts.COMMITTED) btnTxt = 'پذیرش'
    else if (order.state === consts.ACCEPTED) btnTxt = 'ارسال شد'
    else if (order.state === consts.SENT) btnTxt = 'تحویل شد'

    let discountCode = ''


    if (order.discountCode || order.inviterCode)
        discountCode =
            <div className="food-order-text">
                <img className="food-order-icon" src={creditSpent}/>
                {(order.discountCode) ?
                    <>
                        کد تخفیف : {order.discountCode}
                    </>
                    :
                    <>
                        کد معرف : {order.inviterCode}
                    </>
                }
            </div>

    return (
        <div key={order._id} className="order-con">
            <div className='order-layout pos-rltv'>

                <div className="item-head row a-items-c">

                    <div className="close-btn" onClick={handleCancel}/>

                    <div className={`order-badge bd-rd-5 abs-center ${(order.state === consts.COMMITTED) ? 'green': 'gray'}`}>
                        <i className={`fas ${(order.state === consts.COMMITTED)? 'fa-utensils': 'fa-motorcycle'}`}/>
                        <span className='m-r-5'>{order.state}</span>
                    </div>

                    <div className="m-r-a dir-ltr">{order.created}</div>

                </div>

                <div className="item-body">

                    <div className="food-order-text">
                        <img className="food-order-icon" src={food}/>
                        {getFormattedFoods(order.foods)}
                    </div>

                    <hr/>

                    <div className="food-order-text clickable" onClick={() => dispatch(handleModal(consts.MODAL_CUSTOMER, null, [], {phone: order.owner.phone, name: order.owner.name}))}>
                        <img className="food-order-icon" src={avatar}/>
                        {order.owner.name}
                    </div>

                    <div className="food-order-text">
                        <img className="food-order-icon" src={phone}/>
                        {order.phone} | {order.telePhone}
                    </div>

                    <div className="food-order-text clickable" onClick={() => dispatch(handleModal(consts.MODAL_SIMPLE_MAP, null, [], {location: order.location}))}>
                        <img className="food-order-icon" src={location}/>
                        {order.address + ' (فاصله ' + order.distance + ' متر)'}
                    </div>

                    <hr/>

                    <div className="food-order-text">
                        <img className="food-order-icon" src={price}/>
                        قیمت کل : {getFormattedPrice(order.price)} تومان
                    </div>

                    <div className="food-order-text">
                        <img className="food-order-icon" src={motor}/>
                        پیک : {getFormattedPrice(order.cPrice)} تومان
                    </div>

                    {discountCode}

                    <div className="food-order-text">
                        <img className="food-order-icon" src={discount}/>
                        جمع تخفیفات : {getFormattedPrice(order.discounts)} تومان
                    </div>

                    <div className="food-order-text">
                        <img className="food-order-icon" src={creditSpent}/>
                        اعتبار مصرفی : {getFormattedPrice(order.credit)} تومان
                    </div>

                    <div className="food-order-text">
                        <img className="food-order-icon" src={payable}/>

                        {(order.method === consts.ONLINE)?
                            <span className=''>{`پرداخت شده (آنلاین) : ${getFormattedPrice(order.payable)} تومان`}</span>:
                            `قابل پرداخت : ${getFormattedPrice(order.payable)} تومان`
                        }

                    </div>

                    {(order.wantAtm)?
                        <div className="food-order-text">
                            <img className="food-order-icon" src={atm}/>
                            کارتخوان میخواهد
                        </div>
                        :
                        ''
                    }

                    <div className="t-align-c m-t-a" style={{height: '32px'}}>
                        {(loading)?
                            <div className="button m-t-10 m-width-75"><div className='loader'/></div>:
                            <div className="button m-t-10 m-width-75" onClick={() => buttonClick(order._id, order.state)}>{btnTxt}</div>
                        }
                    </div>


                </div>

            </div>
        </div>
    )
}
export default ActiveOrder
