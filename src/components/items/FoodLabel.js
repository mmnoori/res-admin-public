import React, {Component} from 'react';
import {connect} from 'react-redux';

import {log , consts, handleInputChange} from '../../config/config';

import {addFoodLabel, editFoodLabel, deleteFoodLabel, handleSecondModal, showToast} from "../../store/mainActions";


/*
* gets editable, newLabel: boolean, index, foodLabel as props
* */
class FoodLabel extends Component {

    constructor(props) {
        super();

        this.state = {
            editable: (props.editable),
            inputValue: props.foodLabel.name,
            savedLabel: false,
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleAddEdit = this.handleAddEdit.bind(this);
        this.handleDelete= this.handleDelete.bind(this);

        this.handleKey = this.handleKey.bind(this);
    }

    componentDidMount() {

        if (this.props.newLabel) this.foodInput.focus();
    }


    handleChange(event) {
        if (!this.state.editable) return;
        handleInputChange(this, event)
    }

    handleKey(e) {
        if (e.key !== 'Enter') return;
        this.handleAddEdit();
    }

    handleAddEdit() {

        if (this.state.inputValue === '') return this.props.showToast('نام دسته خالی است'); // if input was empty

        // log(this.state.editable);

        this.setState({ editable: !this.state.editable });

        // log(this.state.editable);


        // FOR FUCK SAKE state wont get updated instantly here, so we're checking prevState
        if (!this.state.editable) { // if editable just became true, just set focus and return
            return this.foodInput.focus();
        }


        if (this.state.inputValue === this.props.foodLabel.name) return; // if there was no change

        if (this.props.newLabel && !this.state.savedLabel) { // if save request wasn't already sent
            this.props.addFoodLabel(this.state.inputValue, this.props.sortbarFoodStatus, this.props.sortbarFoodLabel);
            this.setState({savedLabel: this.state.inputValue});

        } else {

            if (this.state.savedLabel)  // if newLabel was saved and now we are editing it
                this.props.editFoodLabel(this.state.savedLabel, this.state.inputValue, this.props.sortbarFoodStatus, this.props.sortbarFoodLabel);
            else
                this.props.editFoodLabel(this.props.foodLabel.name, this.state.inputValue, this.props.sortbarFoodStatus, this.props.sortbarFoodLabel);

            this.setState({savedLabel: this.state.inputValue}) // updating savedLabel
        }

    }

    handleDelete() {

        if (this.props.newLabel && !this.state.saved) // remove whole label
            return this.props.cancelNewLabel(this.props.index);


        if (this.state.editable)
            return this.setState({editable: !this.state.editable, inputValue: this.props.foodLabel.name}); // cancel editing


        this.props.handleConfirmModal(consts.MODAL_CONFIRM, {foodLabel: {name: this.state.inputValue}} );
    }

    render() {

        let secondBtnClass;
        let inputClass;

        if (!this.state.editable) {
            secondBtnClass = "food-lbl-btn food-lbl-edit m-r-5";
            inputClass = "food-label-input bd-rd-5 p-r-3";
        } else {
            secondBtnClass = "food-lbl-btn food-lbl-check m-r-5";
            inputClass = "bd-rd-5 p-r-3";
        }

        return (
            <>
                <div className="food-label">
                    <input className={inputClass} type="text" onChange={this.handleChange} ref={node => this.foodInput=node} name='inputValue'
                           maxLength={consts.FOOD_NAME_MAX_LENGTH} onKeyDown={this.handleKey} value={this.state.inputValue}/>
                    <div className={secondBtnClass} onClick={this.handleAddEdit}/>
                    <div className="food-lbl-btn food-lbl-delete m-r-5" onClick={this.handleDelete}/>
                </div>
            </>
        );
    }
}


const mapStateToProps = state => ({
    sortbarFoodLabel: state.data.foodLabel,
    sortbarFoodStatus: state.data.foodStatus,
});

export default connect(mapStateToProps, {addFoodLabel, editFoodLabel, deleteFoodLabel, handleConfirmModal: handleSecondModal, showToast})(FoodLabel);
