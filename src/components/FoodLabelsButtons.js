import React, {Component} from 'react';
import connect from "react-redux/es/connect/connect";

/*
* gets onClick as props
* */
class FoodLabelsButtons extends Component {


    render() {
        return (
            <>
                {
                    this.props.foodLabels.map(foodLabel => (
                        <button key={foodLabel._id} className="sortbar-dropdown-item" onClick={this.props.handleFoodLabel}>{foodLabel.name}</button>
                    ))
                }
            </>
        );
    }
}

const mapStateToProps = state => ({
    foodLabels: state.data.foodLabels,
});

export default connect(mapStateToProps, {})(FoodLabelsButtons);