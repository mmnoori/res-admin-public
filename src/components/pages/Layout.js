import React, {Component} from 'react'
import { Route, Switch} from 'react-router-dom'
import {connect} from 'react-redux'

import {changeRoute, handleLogged, fetchManagement} from '../../store/mainActions'
import Sidebar from '../Sidebar'
import Header from '../Header'
import Toast from '../Toast'
import expand from '../../assets/icons/expand.svg'
import { baseAddress, log, postRequest } from '../../config/config'

import Home from '../layouts/Home'
import ActiveOrders from '../layouts/ActiveOrders'
import Orders from '../layouts/Orders'
import Customers from '../layouts/Customers'
import Menu from '../layouts/Menu'
import Comments from '../layouts/Comments'
import Reports from '../layouts/Reports'
import Events from '../layouts/Events'
import Management from '../layouts/Management'
import Help from '../layouts/Help'
import ModalManager from "../Modal/ModalManager"
import SecondModal from "../Modal/SecondModal"



class Layout extends Component {

    constructor() {
        super()

        this.state = { }
        this.updateWindowDimensions = this.updateWindowDimensions.bind(this)
    }


    componentDidMount() {
        this.updateWindowDimensions()
        window.addEventListener('resize', this.updateWindowDimensions)

        this.props.changeRoute(this.props.location.pathname) // for first component render
        this.props.fetchManagement() // to get automated acceptOrder
    }


    componentWillUnmount() {
        window.removeEventListener('resize', this.updateWindowDimensions)
    }

    updateWindowDimensions(e) {
        this.setState({ width: window.innerWidth, height: window.innerHeight })
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.logged === false) {
            this.props.history.push('/login')
        }

    }

    // because componentDidMount will be executed only once for next updates we used componentDidUpdate
    componentDidUpdate(prevProps) {
        if (this.props.location.pathname !== prevProps.location.pathname) {
            this.props.changeRoute(this.props.location.pathname) // for change
        }
    }



    render() {

        if (this.state.width < 1100 || this.state.height < 624)
            return(
                <div className="full-screen">
                    <div className="screen" >
                        <img className="expand" src={expand} alt="screen-full"/>
                    </div>

                    <div style={{fontSize: '24px', color: 'white'}}>لطفا اندازه مرورگر را بزرگتر کنید</div>
                </div>
            )


        return (
            <>

                <Sidebar/>

                <div className="page-container">

                    <Header/>

                    <div className="main-content">

                        <Switch>

                            <Route path="/" component={Home} exact/>
                            <Route path="/active-orders" component={ActiveOrders} />
                            <Route path="/orders" component={Orders} />
                            <Route path="/customers" component={Customers}/>
                            <Route path="/menu" component={Menu}/>
                            <Route path="/events" component={Events}/>
                            <Route path="/reports" component={Reports}/>
                            <Route path="/comments" component={Comments}/>
                            <Route path="/management" component={Management}/>
                            <Route path="/help" component={Help}/>

                        </Switch>

                    </div>

                    <ModalManager/>
                    <SecondModal/>

                </div>

                <Toast/>

            </>
        )
    }
}

const mapStateToProps = state => ({
    logged : state.data.logged,
})

export default connect(mapStateToProps, {changeRoute, handleLogged, fetchManagement})(Layout)
