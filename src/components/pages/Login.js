import React, {Component} from 'react';
import {connect} from 'react-redux';

import {URLS, log, postRequest, consts, handleInputChange} from '../../config/config';
import {handleLogged, showToast} from "../../store/mainActions";

import unlockPng from '../../assets/icons/unlock.png';
import Toast from "../Toast";


class Login extends Component {

    constructor() {
        super();

        this.state = {
            username: '',
            password: '',
            alreadyLogged: true
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleKey = this.handleKey.bind(this);
    }

    componentWillMount() {

        if (this.props.logged === false)  // if some previous requests was 401
            return this.setState({alreadyLogged: false});


        postRequest(URLS.ROUTE_CHECK_TOKEN, {}, (data, statusCode) => {
            if (statusCode === consts.SUCCESS_CODE) { // User already Authorized

                this.props.history.push('/');

            } else if (statusCode === consts.UNAUTHORIZED_CODE) { // User Not Authorized

                this.setState({alreadyLogged: false});
            }
        });
    }

    handleKey(e) {
        if (e.key !== 'Enter') return;
        this.handleSubmit();
    }

    handleChange(e) { handleInputChange(this, e) }

    handleSubmit(event) {

        postRequest(URLS.ROUTE_AUTHENTICATE, this.state, (data, statusCode) => {

            if (statusCode === consts.SUCCESS_CODE) {

                // we can use redirect programatically like this or by UI using Link/NavLink
                // but we should be in nested child of a component rendered by the Router
                this.props.history.push('/');

                this.props.handleLogged(true);
            } else
                this.props.showToast('اطلاعات ورودی درست نیست');

        });
    }


    render() {

        // not rendering login form before checking if user is already logged in or not
        if (this.state.alreadyLogged !== false) return '';

        return (
            <div className="login-con dir-rtl">
                <div className="login-wrap">

                    <div className="card login" >

                        <div className="t-align-c m-b-30">
                            <img src={unlockPng} alt="unlock img" style={{height: '120px', width: '120px'}}/>
                        </div>

                        <div className="login-form t-align-r">

                            <div className='m-b-15'>
                                <label>نام کاربری</label>
                                <input className="login-input width-full dir-ltr bd-rd-5" type="username" name="username" maxLength={consts.FOOD_NAME_MAX_LENGTH}
                                       placeholder="Username" onChange={this.handleChange} value={this.state.username}/>
                            </div>

                            <div className='m-b-15'>
                                <label>رمز عبور</label>
                                <input className="login-input width-full dir-ltr bd-rd-5" type="password" name="password" maxLength={consts.FOOD_NAME_MAX_LENGTH}
                                       placeholder="Password" onChange={this.handleChange} value={this.state.password} onKeyDown={this.handleKey}/>
                            </div>

                            <div className="login-checkbox m-b-5">
                                <label className='pos-rltv dis-flex'>
                                    <input className='remember-checkbox m-l-5' type="checkbox" name="remember"/>
                                    <span className='m-r-20'>مرا بخاطر بسپار</span>
                                </label>
                            </div>

                            <button className="au-btn au-btn--block au-btn--green m-b-20" type="submit" onClick={this.handleSubmit}>
                                ورود
                            </button>

                        </div>

                    </div>
                </div>

                <Toast/>

            </div>
        );
    }
}

const mapStateToProps = state => ({
    logged : state.data.logged
});

export default connect(mapStateToProps, {handleLogged, showToast})(Login);
