import React, {Component} from 'react'
import {connect} from 'react-redux'

import {showToast} from '../store/mainActions'
import {CSSTransition} from "react-transition-group"
import {log} from "../config/config"



class Toast extends Component {

    constructor() {
        super()

        this.state = {
            msg: ''
        }

        this.manageToastTimeout = this.manageToastTimeout.bind(this)
    }

    // componentWillUpdate and componentWillReceiveProps won't be triggered when exact same props is sent to them, at least with redux

    componentDidUpdate(prevProps, prevState, snapshot) {

        if (this.props.toast !== false && prevProps.toast === false) { //  we have a new toast
            this.manageToastTimeout()
            this.setState({msg: this.props.toast}) // saving msg in state
        }

    }

    manageToastTimeout() {

        // clear existing timeout
        if (this.toastTimout) clearTimeout(this.toastTimout)

        this.toastTimout = setTimeout(() => {
            this.props.showToast(false)
        }, 3000)
    }


    render() {

        return (
            <CSSTransition
                in={(this.props.toast)? true: false}
                // in={true}
                timeout={{ enter: 300, exit: 300 }}
                classNames="toast"
                unmountOnExit
            >

                <div className="toast-container dir-rtl">
                    {this.state.msg}
                    {/*این برای تست است*/}
                </div>

            </CSSTransition>
        )
    }
}

function demoAsyncCall(latency) {
    return new Promise((resolve) => setTimeout(() => { resolve() }, latency))
}

const mapStateToProps = state => ({
    toast: state.data.toast,
})

export default connect(mapStateToProps, {showToast})(Toast)
