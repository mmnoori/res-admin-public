import React, {Component} from 'react';
import {connect} from 'react-redux';

import {log , consts} from '../../config/config';
import MyCalendar from '../Calendar';
import {fetchOrders} from "../../store/mainActions";


class OrderSortbar extends Component {

    constructor() {
        super();

        this.state = {
            sortDropdown: false,
            statusDropdown: false,
        };

        this.closeDropdown = this.closeDropdown.bind(this);
        this.toggleDropdown = this.toggleDropdown.bind(this);

        this.handleOrderSort = this.handleOrderSort.bind(this);
        this.handleOrderStatus= this.handleOrderStatus.bind(this);
        this.handleOrderDate= this.handleOrderDate.bind(this);

        this.resetState= this.resetState.bind(this);
    }

    componentDidMount() {
        document.addEventListener('mousedown', this.closeDropdown);
    }
    componentWillUnmount() {
        document.removeEventListener('mousedown', this.closeDropdown);
    }

    closeDropdown(event) {

        if (!this.statusDropdown.contains(event.target) && !this.sortDropdown.contains(event.target)) {
            // handle click outside of wrapperRef
            this.resetState();
        }
    }

    toggleDropdown(event) {

        this.resetState();

        if (this.sortBtn.contains(event.target)) this.setState({sortDropdown: !this.state.sortDropdown});

        else if (this.statusBtn.contains(event.target)) this.setState({statusDropdown: !this.state.statusDropdown});

    }

    resetState() {
        this.setState({sortDropdown: false, statusDropdown: false,});
    }


    // limit orders status
    handleOrderStatus(e) {

        this.resetState();
        let status = e.currentTarget.textContent;

        // check if orders are not already sorted with this shit :)
        if (this.props.orderStatus !== status)
            this.props.fetchOrders(0, this.props.orderSort, status, this.props.orderDate);

    }

    // sort orders
    handleOrderSort(e) {

        this.resetState();
        let sort = e.currentTarget.textContent;

        // check if orders are not already sorted with this shit :)
        if (this.props.orderSort !== sort)
            this.props.fetchOrders(0, sort, this.props.orderStatus, this.props.orderDate);
    }

    handleOrderDate(date) {
        this.props.fetchOrders(0, this.props.orderSort, this.props.orderStatus, date);
    }



    render() {

        let sortDropdown = '';
        if (this.state.sortDropdown)
            sortDropdown =
                <div className="sortbar-dropdown t-align-r" style={{ left: '1px'}} >
                    <div className="sortbar-dropdown-item" onClick={this.handleOrderSort}>{consts.LATEST}</div>
                    <div className="sortbar-dropdown-item" onClick={this.handleOrderSort}>{consts.HIGHEST_SUM}</div>
                </div>;


        let statusDropdown = '';
        if (this.state.statusDropdown)
            statusDropdown =
                <div className="sortbar-dropdown t-align-r" style={{ left: '-1px'}}>
                    <div className="sortbar-dropdown-item" onClick={this.handleOrderStatus}>{consts.ALL}</div>
                    <div className="sortbar-dropdown-item" onClick={this.handleOrderStatus}>{consts.REJECTED}</div>
                    <div className="sortbar-dropdown-item" onClick={this.handleOrderStatus}>{consts.DELIVERED}</div>
                </div>;


        return (
            <div className="dis-flex dir-rtl m-b-25">

                <div className="pos-rltv dis-flex " ref={node => this.sortDropdown = node}>

                    <div className="p-l-5 p-t-5">{consts.SORTBAR_SORT_BY}</div>

                    <div className="button grey m-width-100"onClick={this.toggleDropdown} ref={node => this.sortBtn = node}>
                        {this.props.orderSort}
                    </div>

                    {sortDropdown}
                </div>


                <div className="pos-rltv dis-flex m-r-10" ref={node => this.statusDropdown = node}>

                    <div className="p-l-5 p-t-5">{consts.SORTBAR_STATUS}</div>

                    <div className="button grey m-width-100" onClick={this.toggleDropdown} ref={node => this.statusBtn = node} >
                        {this.props.orderStatus}
                    </div>

                    {statusDropdown}
                </div>

                <div className="pos-rltv dis-flex m-r-45" >

                    <MyCalendar currentDate={this.props.orderDate} onDateChange={this.handleOrderDate} removable={true}/>

                </div>

            </div>
        );
    }
}


const mapStateToProps = state => ({
    orderSort: state.data.orderSort,
    orderStatus: state.data.orderStatus,
    orderDate: state.data.orderDate,
});

export default connect(mapStateToProps, {fetchOrders})(OrderSortbar);
