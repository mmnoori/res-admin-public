import React, {Component} from 'react';
import {connect} from 'react-redux';

import {fetchCustomers, handleModal, showToast, handleSecondModal} from "../../store/mainActions";
import { log , consts} from '../../config/config';


/*
* gets openModalCondition: boolean, selected customers as props
* */
class CustomerSortbar extends Component {

    constructor() {
        super();

        this.state = {
            sortDropdown: false,
            statusDropdown: false,
        };

        this.closeDropdown = this.closeDropdown.bind(this);
        this.toggleDropdown = this.toggleDropdown.bind(this);

        this.handleCustomerSort = this.handleCustomerSort.bind(this);
        this.handleCustomerStatus = this.handleCustomerStatus.bind(this);
        this.handleDiscount = this.handleDiscount.bind(this);

        this.resetState = this.resetState.bind(this);
    }

    componentDidMount() {
        document.addEventListener('mousedown', this.closeDropdown);
    }
    componentWillUnmount() {
        document.removeEventListener('mousedown', this.closeDropdown);
    }

    closeDropdown(event) {

        if (!this.statusDropdown.contains(event.target) && !this.sortDropdown.contains(event.target)) {
            // handle click outside of wrapperRef
            this.resetState();
        }
    }

    handleDiscount(event) {

        // if at least one customer was selected
        if (this.props.openModalCondition) {

            if (this.props.customerStatus === consts.BANNED)
                return this.props.showToast('امکان ارسال کد تخفیف برای کاربران مسدود شده وجود ندارد')

            this.props.handleModal(consts.MODAL_DISCOUNT, null, null, {customers: this.props.customers});
        }

        else
            this.props.showToast('هیچ کاربری انتخاب نشده');
    }

    toggleDropdown(event) {

        this.resetState();

        if (this.sortBtn.contains(event.target)) {

            let dropState = !this.state.sortDropdown;
            this.setState({ sortDropdown: dropState });

        } else if (this.statusBtn.contains(event.target)) {

            let dropState = !this.state.statusDropdown;
            this.setState({ statusDropdown: dropState });
        }
    }

    // limit customers status
    handleCustomerStatus(e) {

        this.resetState();

        const status = e.currentTarget.textContent;

        // check if orders are not already sorted with this shit :)
        if (this.props.customerStatus !== status)
            this.props.fetchCustomers(0, this.props.customerSort, status, this.props.customerSearchText, consts.ORDERS_CHUNK_COUNT);

    }

    // sort customers
    handleCustomerSort(e) {

        this.resetState();

        const sort = e.currentTarget.textContent;

        // check if customers are not already sorted with this shit :)
        if (this.props.customerSort !== sort)
            this.props.fetchCustomers(0, sort, this.props.customerStatus, this.props.customerSearchText, consts.ORDERS_CHUNK_COUNT);
    }

    resetState() {
        this.setState({sortDropdown: false, statusDropdown: false});
    }


    render() {
        let statusDropdown = '';
        if (this.state.statusDropdown)
            statusDropdown =
                <div className="sortbar-dropdown t-align-r" style={{left: '0'}}>
                    <div className="sortbar-dropdown-item" onClick={this.handleCustomerStatus}>{consts.VALID}</div>
                    <div className="sortbar-dropdown-item" onClick={this.handleCustomerStatus}>{consts.BORN_TODAY}</div>
                    <div className="sortbar-dropdown-item" onClick={this.handleCustomerStatus}>{consts.BANNED}</div>
                </div>;


        let sortDropdown = '';
        if (this.state.sortDropdown)
            sortDropdown =
                <div className="sortbar-dropdown t-align-r" style={{ left: '-8px'}} >
                    <div className="sortbar-dropdown-item" onClick={this.handleCustomerSort}>{consts.LATEST}</div>
                    <div className="sortbar-dropdown-item" onClick={this.handleCustomerSort}>{consts.HIGHEST_ORDERS}</div>
                    <div className="sortbar-dropdown-item" onClick={this.handleCustomerSort}>{consts.HIGHEST_INVITES}</div>
                </div>;

        return (
            <div className="dis-flex m-b-25">

                <div className="pos-rltv dis-flex " ref={node => this.statusDropdown = node}>

                    <div className="p-l-5 p-t-5">{consts.SORTBAR_STATUS}</div>

                    <div className="button grey m-width-100" onClick={this.toggleDropdown} ref={node => this.statusBtn = node} >
                        {this.props.customerStatus}
                    </div>

                    {statusDropdown}
                </div>


                <div className="pos-rltv dis-flex " ref={node => this.sortDropdown = node}>

                    <div className="p-l-5 p-t-5 m-r-10">{consts.SORTBAR_SORT_BY}</div>

                    <div className="button grey m-width-100" onClick={this.toggleDropdown} ref={node => this.sortBtn = node} >
                        {this.props.customerSort}
                    </div>

                    {sortDropdown}
                </div>



                <div className="button m-r-45" onClick={this.handleDiscount}>
                    ارسال کد تخفیف
                </div>

            </div>
        );
    }
}


const mapStateToProps = state => ({
    customerSort: state.data.customerSort,
    customerStatus: state.data.customerStatus,
    customerSearchText: state.data.customerSearchText
});

export default connect(mapStateToProps, {fetchCustomers, handleModal, showToast, handleConfirmModal: handleSecondModal})(CustomerSortbar);
