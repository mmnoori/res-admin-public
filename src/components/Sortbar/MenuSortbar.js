import React, {Component} from 'react';
import connect from "react-redux/es/connect/connect";

import {consts, log} from "../../config/config";
import {fetchFoods, handleModal} from "../../store/mainActions";


class MenuSortbar extends Component {

    constructor() {
        super();

        this.state = {
            labelDropdown: false,
            statusDropdown: false,
        };

        this.closeDropdown = this.closeDropdown.bind(this);
        this.toggleDropdown = this.toggleDropdown.bind(this);

        this.handleFoodLabel = this.handleFoodLabel.bind(this);
        this.handleFoodStatus = this.handleFoodStatus.bind(this);
        this.handleClick = this.handleClick.bind(this);

        this.resetState = this.resetState.bind(this);
    }

    componentDidMount() {
        document.addEventListener('mousedown', this.closeDropdown);
    }
    componentWillUnmount() {
        document.removeEventListener('mousedown', this.closeDropdown);
    }

    closeDropdown(event) {

        // handle click outside of wrapperRef
        if (!this.statusDropdown.contains(event.target) && !this.labelDropdown.contains(event.target))
            this.resetState();
    }


    handleClick(event) {

        if (this.newFoodBtn.contains(event.target)) {
            this.props.handleModal(consts.MODAL_FOOD);

        } else if (this.groupDiscountBtn.contains(event.target)) {
            this.props.handleModal(consts.MODAL_FOOD_GROUP_DISCOUNT);

        } else if (this.foodLabelBtn.contains(event.target)) {
            this.props.handleModal(consts.MODAL_LABELS);

        } else if (this.mealBtn.contains(event.target)) {
            this.props.handleModal(consts.MODAL_MEALS);
        }
    }


    toggleDropdown(event) {

        this.resetState();

        if (this.labelBtn.contains(event.target))
            this.setState({ labelDropdown: !this.state.labelDropdown });

        else if (this.statusBtn.contains(event.target))
            this.setState({ statusDropdown: !this.state.statusDropdown });
    }


    // limit customers status
    handleFoodStatus(e) {

        this.resetState();

        let status = e.currentTarget.textContent;

        if (this.props.foodStatus !== status)
            this.props.fetchFoods(status, this.props.foodLabel);

    }

    handleFoodLabel(e) {

        this.resetState();
        let label = e.currentTarget.textContent;

        if (this.props.foodLabel !== label)
            this.props.fetchFoods(this.props.foodStatus, label);
    }

    resetState() {
        this.setState({ labelDropdown: false, statusDropdown: false });
    }



    render() {

        let foodLabelsBtn = this.props.foodLabels.map((foodLabel) => (
            <div key={foodLabel._id} className="sortbar-dropdown-item" onClick={this.handleFoodLabel}>{foodLabel.name}</div>
        ));

        let labelDropdown = '';
        if (this.state.labelDropdown)
            labelDropdown =
                <div className="sortbar-dropdown t-align-r" style={{ left: '0'}} >
                    <div className="sortbar-dropdown-item" onClick={this.handleFoodLabel}>{consts.ALL}</div>
                    {foodLabelsBtn}
                </div>;


        let statusDropdown = '';
        if (this.state.statusDropdown)
            statusDropdown =
                <div className="sortbar-dropdown t-align-r" style={{ left: '0'}}>
                    <div className="sortbar-dropdown-item" onClick={this.handleFoodStatus}>{consts.ALL}</div>
                    <div className="sortbar-dropdown-item" onClick={this.handleFoodStatus}>{consts.AVAILABLE}</div>
                    <div className="sortbar-dropdown-item" onClick={this.handleFoodStatus}>{consts.NOT_AVAILABLE}</div>
                </div>;


        return (
            <div className="dis-flex m-b-25">

                <div className="pos-rltv dis-flex" ref={node => this.statusDropdown = node}>

                    <div className="p-l-5 p-t-5">{consts.SORTBAR_STATUS}</div>

                    <button className="button grey m-width-100" onClick={this.toggleDropdown} ref={node => this.statusBtn = node} >
                        {this.props.foodStatus}
                    </button>

                    {statusDropdown}
                </div>

                <div className="pos-rltv dis-flex" ref={node => this.labelDropdown = node}>

                    <div className="p-l-5 p-t-5 m-r-10">{consts.SORTBAR_LABEL}</div>

                    <div className="button grey m-width-100" onClick={this.toggleDropdown} ref={node => this.labelBtn = node} >
                        {this.props.foodLabel}
                    </div>

                    {labelDropdown}
                </div>

                <button className="button m-r-45" type="button" ref={node => this.newFoodBtn = node} onClick={this.handleClick}>
                    غذای جدید
                </button>

                <button className="button m-r-15" type="button" ref={node => this.groupDiscountBtn = node} onClick={this.handleClick}>
                    تخفیف گروهی
                </button>

                <button className="button m-r-15" type="button" ref={node => this.foodLabelBtn = node} onClick={this.handleClick}>
                    دسته ها
                </button>

                <button className="button m-r-15" type="button" ref={node => this.mealBtn = node} onClick={this.handleClick}>
                    وعده ها
                </button>

            </div>
        );
    }
}

const mapStateToProps = state => ({
    foodLabel: state.data.foodLabel,
    foodLabels: state.data.foodLabels,
    foodStatus: state.data.foodStatus,
});

export default connect(mapStateToProps, {fetchFoods, handleModal})(MenuSortbar);
