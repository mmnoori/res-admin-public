import {baseAddress, log, postRequest, URLS, consts, getRequest,} from "../config/config"


export const types =  {
    FETCH_CUSTOMERS: 'FETCH_CUSTOMERS',
    PUSH_CUSTOMERS: 'PUSH_CUSTOMERS',

    FETCH_ORDERS: 'FETCH_ORDERS',
    PUSH_ORDERS: 'PUSH_ORDERS',

    FETCH_REPORTS: 'FETCH_REPORTS',
    PUSH_REPORTS: 'PUSH_REPORTS',

    FETCH_COMMENTS: 'FETCH_COMMENTS',
    PUSH_COMMENTS: 'PUSH_COMMENTS',

    FETCH_EVENTS: 'FETCH_EVENTS',
    PUSH_EVENTS: 'PUSH_EVENTS',

    FETCH_FOODS: 'FETCH_FOODS',
    FETCH_ACTIVE_ORDERS: 'FETCH_ACTIVE_ORDERS',

    UPDATE_FOOD_LIST: 'UPDATE_FOOD_LIST',
    DELETE_FOOD: 'DELETE_FOOD',

    UPDATE_MEALS: 'UPDATE_MEALS',

    FETCH_MANAGEMENT: 'FETCH MANAGEMENT',

    ADD_FOOD_LABEL: 'ADD_FOOD_LABEL',
    EDIT_FOOD_LABEL: 'EDIT_FOOD_LABEL',
    DELETE_FOOD_LABEL: 'DELETE_FOOD_LABEL',


    ORDER_SORT: 'ORDER_SORT',
    ORDER_STATUS: 'ORDER_STATUS',

    CHANGE_ROUTE: 'CHANGE_ROUTE',

    HANDLE_MODAL: 'HANDLE_MODAL',
    CLOSE_MODAL: 'CLOSE_MODAL',

    HANDLE_SECOND_MODAL: 'HANDLE_SECOND_MODAL',
    CLOSE_SECOND_MODAL: 'CLOSE_SECOND_MODAL',

    HANDLE_TOAST: 'HANDLE_TOAST',

    HANDLE_REQ_IN_PROCESS: 'HANDLE_PROCESS',


    LOGGED: 'LOGGED'
}





export const handleLogged = (arg) => dispatch => {
    dispatch({type: types.LOGGED, payload: arg })
}
export const handleModal = (payload, stackToAdd, totalStack, modalRepo) => dispatch => { // can pass null if dont wanna change previous vaule of stackToAdd/totalStack/modalRepo
    dispatch({ type: types.HANDLE_MODAL, payload, stackToAdd, totalStack, modalRepo })
}
export const closeModal = () => dispatch => { // closes modal and wipes backStackModals & modalRepo
    dispatch({type: types.CLOSE_MODAL})
}
export const handleSecondModal = (payload, secondModalRepo) => dispatch => { // this is can be our secondary Modal
    dispatch({ type: types.HANDLE_SECOND_MODAL, payload, secondModalRepo })
}
export const closeSecondModal = () => dispatch => {
    dispatch({type: types.CLOSE_SECOND_MODAL})
}
export const changeRoute = (currentRoute) => dispatch => {
    dispatch({type: types.CHANGE_ROUTE, payload: currentRoute})
}
export const showToast = (msg) => dispatch => { // msg can be String or false
    dispatch({type: types.HANDLE_TOAST, payload: msg })
}
export const handleReqProcess = (arg) => dispatch => {
    return dispatch({ type: types.HANDLE_REQ_IN_PROCESS, payload: arg })
}

export const fetchCustomers = function(skipCount, sortBy, status, searchText, limit) {

    return function (dispatch) {

        postRequest(URLS.ROUTE_GET_CUSTOMERS, {skipCount, sortBy, state: status, searchText, limit}, (res, statusCode) => {

            if (checkStatusCode(dispatch, res, statusCode) !== true) return

            // handle actionType
            let type
            if (skipCount === 0) type = types.FETCH_CUSTOMERS
            else type = types.PUSH_CUSTOMERS

            dispatch({
                type,
                payload: res,
                customerSort: sortBy,
                customerStatus: status,
                customerSearchText: searchText,
                listEnd: (res.customers.length === 0 || res.customers.length%consts.CUSTOMERS_CHUNK_COUNT !== 0)
            })
        })
    }
}

export const banCustomer = function(_id, banned, skipCount, sortBy, status, searchText, limit) {

    return function (dispatch) {

        postRequest(URLS.ROUTE_BAN_CUSTOMER, {_id, banned, skipCount, sortBy, state: status, searchText, limit}, (res, statusCode) => {

            if (checkStatusCode(dispatch, res, statusCode) !== true) return

            dispatch({
                type: types.FETCH_CUSTOMERS,
                payload: res,
                customerSort: sortBy,
                customerStatus: status,
                customerSearchText: searchText,
                listEnd: (res.customers.length === 0 || res.customers.length%consts.CUSTOMERS_CHUNK_COUNT !== 0),
                secondModal: false,
                showModal: false
            })
        })
    }
}

export const fetchOrders = (skipCount, sortBy, status, date, searchText, limit) => dispatch => {

    let dateToSend = (date === '')? undefined: date // send date if selected

    postRequest(URLS.ROUTE_GET_ORDERS, { skipCount, sortBy, state: status, date: dateToSend, searchText, limit}, (res, statusCode) => {

        if (checkStatusCode(dispatch, res, statusCode) !== true) return

        let type
        if (skipCount === 0) type = types.FETCH_ORDERS
        else type = types.PUSH_ORDERS

        dispatch({
            type,
            payload: res,
            orderSort: sortBy,
            orderStatus: status,
            orderDate: date,
            listEnd: (res.orders.length === 0 || res.orders.length%consts.ORDERS_CHUNK_COUNT !== 0)
        })
    })
}

export const fetchFoods = (status, label) => dispatch => { // get food labels too

    postRequest(URLS.ROUTE_GET_FOODS, {state: status, label: label}, (res, statusCode) => {

        if (checkStatusCode(dispatch, res, statusCode) !== true) return

        dispatch({
            type: types.FETCH_FOODS,
            payload: res,
            foodStatus: status,
            foodLabel: label
        })
    })
}

export const fetchActiveOrders = () => dispatch => {

    postRequest(URLS.ROUTE_GET_ORDERS, {state: consts.ACTIVE_ORDERS, sortBy: consts.LATEST}, (res, statusCode) => {

        if (checkStatusCode(dispatch, res, statusCode) !== true) return

        dispatch({
            type: types.FETCH_ACTIVE_ORDERS,
            payload: res,
        })
    })
}

export const fetchReports = function(skipCount, limit) {

    return function (dispatch) {

        postRequest(URLS.ROUTE_GET_REPORTS, {skipCount, limit}, (res, statusCode) => {

            if (checkStatusCode(dispatch, res, statusCode) !== true) return

            let type
            if (skipCount === 0) type = types.FETCH_REPORTS
            else type = types.PUSH_REPORTS

            dispatch({
                type,
                payload: res,
                listEnd: (res.reports.length === 0 || res.reports.length%consts.REPORTS_CHUNK_COUNT !== 0)
            })
        })
    }
}

export const fetchComments = function(skipCount, limit) {

    return function (dispatch) {

        postRequest(URLS.ROUTE_GET_COMMENTS, {skipCount, limit}, (res, statusCode) => {

            if (checkStatusCode(dispatch, res, statusCode) !== true) return

            let type
            if (skipCount === 0) type = types.FETCH_COMMENTS
            else type = types.PUSH_COMMENTS

            dispatch({
                type,
                payload: res,
                listEnd: (res.comments.length === 0 || res.comments.length%consts.COMMENTS_CHUNK_COUNT !== 0)
            })
        })
    }
}

export const fetchEvents = (skipCount, limit) => dispatch => { // get food labels too

    postRequest(URLS.ROUTE_GET_EVENTS, {skipCount, limit}, (res, statusCode) => {

        if (checkStatusCode(dispatch, res, statusCode) !== true) return

        let type
        if (skipCount === 0) type = types.FETCH_EVENTS
        else type = types.PUSH_EVENTS

        dispatch({
            type,
            payload: res,
            listEnd: (res.events.length === 0 || res.events.length%consts.EVENTS_CHUNK_COUNT !== 0)
        })
    })
}

export const updateFood = (food, img, status, label) => dispatch => {

    let formData = new FormData()
    formData.append('state', status)
    formData.append('label', label)
    formData.append('img', img)
    formData.append('food', JSON.stringify(food))

    postRequest(URLS.ROUTE_UPDATE_FOOD, formData, (res, statusCode) => {

        if (checkStatusCode(dispatch, res, statusCode) !== true) return

        dispatch({
            type: types.UPDATE_FOOD_LIST,
            payload: res,
            showModal: false
        })
    })
}

export const deleteFood = (id, status, label) => dispatch => {

    postRequest(URLS.ROUTE_DELETE_FOOD, {_id: id, state: status, label: label}, (res, statusCode) => {

        if (checkStatusCode(dispatch, res, statusCode) !== true) return

        dispatch({
            type: types.UPDATE_FOOD_LIST,
            payload: res,
            secondModal: false
        })
    })
}

export const updateMeals = ({lsh, lsm, leh, lem, dsh, dsm, deh, dem, hasTwoMeals}) => dispatch => {

    postRequest(URLS.ROUTE_UPDATE_MEALS, {lsh, lsm, leh, lem, dsh, dsm, deh, dem, hasTwoMeals}, (res, statusCode) => {

        if (checkStatusCode(dispatch, res, statusCode) !== true) return

        dispatch({
            type: types.UPDATE_MEALS,
            payload: res,
        })
    })
}


export const groupFoodDiscount = (discountLabel, discount, suggestedOnly, availableOnly, status, label) => dispatch => {

    postRequest(URLS.ROUTE_FOOD_GROUP_DISCOUNT,
        {discountLabel: discountLabel, discount: discount, suggestedOnly: suggestedOnly, availableOnly: availableOnly, label: label, state: status},
        (res, statusCode) => {

        if (checkStatusCode(dispatch, res, statusCode) !== true) return

        dispatch({
            type: types.UPDATE_FOOD_LIST,
            payload: res,
            showModal: false
        })
    })
}

export const addFoodLabel = (name, status, label) => dispatch => {

    postRequest(URLS.ROUTE_ADD_FOOD_LABEL, {name, state: status, label: label}, (res, statusCode) => {

        if (checkStatusCode(dispatch, res, statusCode) !== true) return

        dispatch({
            type: types.UPDATE_FOOD_LIST,
            payload: res,
            showModal: true
        })
    })
}

export const editFoodLabel = (prevName, newName, status, label) => dispatch => {

    postRequest(URLS.ROUTE_EDIT_FOOD_LABEL, {prevName, newName, state: status, label: label}, (res, statusCode) => {

        if (checkStatusCode(dispatch, res, statusCode) !== true) return

        dispatch({
            type: types.UPDATE_FOOD_LIST,
            payload: res,
            showModal: true
        })
    })
}

export const deleteFoodLabel = (name, status, label) => dispatch => {

    postRequest(URLS.ROUTE_DELETE_FOOD_LABEL, {name, state: status, label: label}, (res, statusCode) => {

        if (checkStatusCode(dispatch, res, statusCode) !== true) return

        dispatch({
            type: types.UPDATE_FOOD_LIST,
            payload: res,
            showModal: true,
            secondModal: false
        })
    })
}

export const fetchManagement = () => dispatch => {

    return postRequest(URLS.ROUTE_MANAGEMENT, {}, (res, statusCode) => {

        if (checkStatusCode(dispatch, res, statusCode) !== true) return

        dispatch({
            type: types.FETCH_MANAGEMENT,
            payload: res,
        })
    })
}

export const updateManagement = (params) => dispatch => {

    // (courier, location, dfo, maxDistance, leastPrice, acceptOrder, phone)

    return postRequest(URLS.ROUTE_UPDATE_MANAGEMENT, params, (res, statusCode) => {

        if (checkStatusCode(dispatch, res, statusCode) !== true) return

        dispatch({
            type: types.FETCH_MANAGEMENT,
            payload: res,
            showModal: false,
            secondModal: false
        })
    })
}

export const handleOrder = (orderId, state, ownerPhone, code) => dispatch => {

    return postRequest(URLS.ROUTE_HANDLE_ORDER, {orderId, state, ownerPhone}, (res, statusCode) => {

        if (checkStatusCode(dispatch, res, statusCode) !== true) return

        if (state === consts.REJECTED) {
            dispatch({type: types.HANDLE_TOAST, payload: 'سفارش ' + code + ' لغو شد'})
        }

        dispatch({
            type: types.FETCH_ACTIVE_ORDERS,
            payload: res,
            secondModal: false
        })
    })
}




export const updateEvent = (event, img) => dispatch => {

    let formData = new FormData()
    formData.append('img', img)
    formData.append('event', JSON.stringify(event))
    formData.append('skipCount', 0)
    formData.append('limit', consts.EVENTS_CHUNK_COUNT)

    postRequest(URLS.ROUTE_UPDATE_EVENT, formData, (res, statusCode) => {

        if (checkStatusCode(dispatch, res, statusCode) !== true) return

        dispatch({
            type: types.FETCH_EVENTS,
            payload: res,
            listEnd: (res.events.length === 0 || res.events.length%consts.EVENTS_CHUNK_COUNT !== 0)
        })
    })
}

export const deleteEvent = (id) => dispatch => {

    postRequest(URLS.ROUTE_DELETE_EVENT, { _id: id, skipCount: 0, limit: consts.EVENTS_CHUNK_COUNT }, (res, statusCode) => {

        if (checkStatusCode(dispatch, res, statusCode) !== true) return

        dispatch({
            type: types.FETCH_EVENTS,
            payload: res,
            listEnd: (res.events.length === 0 || res.events.length%consts.EVENTS_CHUNK_COUNT !== 0),
            secondModal: false
        })
    })
}








export function checkStatusCode(dispatch, response, statusCode) {
    if (statusCode === consts.UNAUTHORIZED_CODE) return window.location.assign("/login") // hard assign
    else if (statusCode === consts.INT_ERR_CODE) return dispatch({type: types.HANDLE_TOAST, payload: 'خطای سرور: 500', reqInProcess: false})
    else if (statusCode === consts.NOT_FOUND_CODE || statusCode === consts.BAD_REQ_CODE) return dispatch({type: types.HANDLE_TOAST, payload: response, reqInProcess: false})
    else return true
}

// returns type: checked if there is no error, handles error if there was any
export const handleStatusCode = (response, statusCode) => {

    if (statusCode === consts.UNAUTHORIZED_CODE) return window.location.assign("/login") // hard assign
    else if (statusCode === consts.INT_ERR_CODE) return dispatch => dispatch({type: types.HANDLE_TOAST, payload: 'خطای سرور: 500', reqInProcess: false})
    else if (statusCode === consts.NOT_FOUND_CODE || statusCode === consts.BAD_REQ_CODE) return dispatch => dispatch({type: types.HANDLE_TOAST, payload: response, reqInProcess: false})
    else return dispatch => dispatch({type: consts.CODE_CHECKED})
    // can't just return true cause redux throws err actions must be plain objects, so we're returning an action with no reducer
        // Actions may not have an undefined "type" property.
}

// NOTE: actions must be return plain objects and cannot be async
export const safeRequest = (url, params, callback, handleStatucCodeAsAction) => {

    return dispatch => {

        postRequest(url, params, (data, statusCode) => {

            if (handleStatucCodeAsAction(data, statusCode).type !== consts.CODE_CHECKED) return

            callback(data, statusCode) // status code was checked successfully
            // doing this just to return something because this we're using function as an redux action
            dispatch({type: consts.CODE_CHECKED})
        })
    }
}
