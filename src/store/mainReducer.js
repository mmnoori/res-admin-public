import {combineReducers} from 'redux'

import {types} from "./mainActions"
import {log, consts} from '../config/config'


const initialState = {
    currentRoute: '',

    showModal: false,
    backStackModals: [],
    modalRepo: {}, // this is for sending information to another modal, without directly using props or using a variable in config

    secondModal: false, // we sometimes use it as second modal and sometimes not
    secondModalRepo: {},

    reqInProcess: false,

    toast: false,
    logged: true,

    automatedOrder: false,
    location: '',
    address: '',
    courier: {},
    maxDistance: '',
    leastPrice: '',
    dfo: '',
    bills: [],
    phone: '',
    invitation: {},


    activeOrders: [],
    activeOrdersListVersion: 0,

    customers: [],
    customerSort: consts.LATEST,
    customerStatus: consts.VALID,
    customerSearchText: '',
    customerEnd: false,

    orders: [],
    orderSort: consts.LATEST,
    orderStatus: consts.ALL,
    orderSearchText: '',
    orderDate: '',
    orderEnd: false,

    reports: [],
    reportEnd: false,

    comments: [],
    commentEnd: false,

    foods: [],
    foodLabels: [],
    foodStatus: consts.ALL,
    foodLabel: consts.ALL,
    foodListVersion: 0,

    events: [],
    eventEnd: false,

    meals: {
        hasTwoMeals: consts.ON,
        lsh: '',
        lsm: '',
        leh: '',
        lem: '',
        dsh: '',
        dsm: '',
        deh: '',
        dem: '',
    }

}

const mainReducer = (state = initialState, action) => {

    let newState = {}

    switch (action.type) {

        case types.LOGGED:

            return {
                // ...initialState, // resetting store on logging changes
                ...state, // resetting store on logging changes
                logged: action.payload
            }

        case types.HANDLE_MODAL:

            let backStackModals
            let modalRepo = state.modalRepo // keeping old modalRepo value if new value is not passed

            if (action.stackToAdd)
                state.backStackModals.push(action.stackToAdd) // push function does not returns array length

            else if (action.totalStack)
                backStackModals = action.totalStack

            else // if there is no stackToAdd or totalStack passed, do not change backStackModals
                backStackModals = state.backStackModals


            if (action.modalRepo) // if any modalRepo is passed
                modalRepo = action.modalRepo

            return {
                ...state, // current state
                showModal: (action.payload === undefined || action.payload === '') ? true : action.payload,
                backStackModals: (backStackModals) ? backStackModals : state.backStackModals,
                modalRepo
            }

        case types.CLOSE_MODAL:
            return {
                ...state,
                ...getCloseModalState()
            }

        case types.HANDLE_SECOND_MODAL:

            let secondModalRepo = state.secondModalRepo // keeping old modalRepo value if new value is not passed

            if (action.secondModalRepo) // if any modalRepo is passed
                secondModalRepo = action.secondModalRepo

            return {
                ...state, // current state
                secondModal: action.payload,
                secondModalRepo
            }

        case types.CLOSE_SECOND_MODAL:
            return {
                ...state,
                secondModal: false,
                secondModalRepo: {}
            }

        case types.CHANGE_ROUTE:

            return {
                ...state,
                ...getInitialListStates(), // wipe customers, orders states
                currentRoute: action.payload
            }

        case types.HANDLE_TOAST:

            return {
                ...state,
                toast: action.payload,
                reqInProcess: (action.reqInProcess !== null) ? action.reqInProcess : state.reqInProcess,
            }

        case types.FETCH_CUSTOMERS:

            if (action.showModal === false) newState = {...getCloseModalState()} // close secondModal
            if (action.secondModal === false) newState = {...newState, ...getCloseSecondModalState()} // close secondModal

            return {
                ...state, // Rest Parameter
                ...newState,
                customers: action.payload.customers,
                customerSort: action.customerSort,
                customerStatus: action.customerStatus,
                customerSearchText: action.customerSearchText,
                customerEnd: action.listEnd,
                reqInProcess: false
            }

        case types.PUSH_CUSTOMERS:

            return {
                ...state,
                customers: state.customers.concat(action.payload.customers),
                customerEnd: action.listEnd,
                reqInProcess: false
            }

        case types.FETCH_ORDERS:

            if (action.secondModal === false) newState = {...getCloseSecondModalState()} // close secondModal

            return {
                ...state,
                ...newState,
                orders: action.payload.orders,
                orderSort: action.orderSort,
                orderStatus: action.orderStatus,
                orderDate: action.orderDate,
                orderEnd: action.listEnd,
                reqInProcess: false
            }

        case types.PUSH_ORDERS:

            return {
                ...state,
                orders: state.orders.concat(action.payload.orders),
                orderEnd: action.listEnd,
                reqInProcess: false
            }

        case types.FETCH_FOODS:

            return {
                ...state,
                foods: action.payload.foods,
                foodLabels: action.payload.foodLabels,
                foodListVersion: action.payload.flv,
                foodStatus: action.foodStatus,
                foodLabel: action.foodLabel,
                ...getMealsFromActionPayload(action.payload),

            }

        case types.FETCH_ACTIVE_ORDERS:

            if (action.secondModal === false) newState = {...newState, ...getCloseSecondModalState()} // close secondModal

            return {
                ...state,
                ...newState,
                activeOrders: action.payload.orders, // cause we're using same api for orders nad activeOrders
                activeOrdersListVersion: action.payload.aolv
            }

        case types.FETCH_REPORTS:

            return {
                ...state, // Rest Parameter
                reports: action.payload.reports,
                reportEnd: action.listEnd,
                reqInProcess: false
            }

        case types.PUSH_REPORTS:

            return {
                ...state,
                reports: state.reports.concat(action.payload.reports),
                reportEnd: action.listEnd,
                reqInProcess: false
            }

        case types.FETCH_COMMENTS:

            return {
                ...state, // Rest Parameter
                comments: action.payload.comments,
                commentEnd: action.listEnd,
                reqInProcess: false
            }

        case types.PUSH_COMMENTS:

            return {
                ...state,
                comments: state.comments.concat(action.payload.comments),
                commentEnd: action.listEnd,
                reqInProcess: false
            }

        case types.FETCH_EVENTS:

            if (action.secondModal === false) newState = {...getCloseSecondModalState()} // close secondModal

            return {
                ...state,
                ...newState,
                ...getCloseModalState(),
                events: action.payload.events,
                eventEnd: action.listEnd,
                reqInProcess: false
            }

        case types.PUSH_EVENTS:

            return {
                ...state,
                events: state.events.concat(action.payload.events),
                eventEnd: action.listEnd,
                reqInProcess: false
            }

        case types.UPDATE_FOOD_LIST:

            // if no showModal argument was sent
            if (action.showModal === null || action.showModal === false) newState = getCloseModalState()
            if (action.secondModal === false) newState = {...newState, ...getCloseSecondModalState()} // close secondModal

            return {
                ...state,
                ...newState, // must be after state to override it
                foods: action.payload.foods,
                foodLabels: action.payload.foodLabels,
                foodListVersion: action.payload.flv,
                // showModal: (action.showModal)? state.showModal: false, // in some cases like add/edit/delete FoodLabel need to keep showModal state
                ...getMealsFromActionPayload(action.payload),

                reqInProcess: false
            }


        case types.UPDATE_MEALS:

            return {
                ...state,
                ...getMealsFromActionPayload(action.payload),
                foodListVersion: state.foodListVersion + 1,
                showModal: false,
                reqInProcess: false
            }

        case types.ADD_FOOD_LABEL:

            return {
                ...state,
                foodLabels: state.foodLabels.concat(action.foodLabel),
                foodListVersion: state.foodListVersion + 1,
            }

        case types.FETCH_MANAGEMENT:

            if (action.secondModal === false) newState = {...getCloseSecondModalState()} // close secondModal

            return {
                ...state,
                ...newState,

                location: action.payload.location,
                address: action.payload.address,
                phone: action.payload.phone,
                courier: action.payload.courier,
                leastPrice: action.payload.leastPrice,
                maxDistance: action.payload.maxDistance,
                dfo: action.payload.dfo,
                bills: action.payload.bills,
                invitation: action.payload.invitation,
                automatedOrder: action.payload.acceptOrder !== consts.OFF, // it is on unless value is consts.OFF

                showModal: (action.showModal !== undefined) ? action.showModal : state.showModal,
            }

        case types.HANDLE_REQ_IN_PROCESS:
            return {
                ...state,
                reqInProcess: action.payload,
            }



        default:
            return state
    }
}

function getCloseModalState() {
    return {
        showModal: false,
        backStackModals: [],
        modalRepo: {}
    }
}

function getCloseSecondModalState() {
    return {
        secondModal: false,
        secondModalRepo: {}
    }
}

function getInitialListStates() {
    return {
        customers: [],
        customerSort: consts.LATEST,
        customerStatus: consts.VALID,
        customerSearchText: '',

        orders: [],
        orderSort: consts.LATEST,
        orderStatus: consts.ALL,
        orderSearchText: '',
        orderDate: '',
    }
}

function getMealsFromActionPayload(payload) {
    return {
        meals: {
            hasTwoMeals: payload.meals.hasTwoMeals,
            lsh: payload.meals.lsh,
            lsm: payload.meals.lsm,
            leh: payload.meals.leh,
            lem: payload.meals.lem,
            dsh: payload.meals.dsh,
            dsm: payload.meals.dsm,
            deh: payload.meals.deh,
            dem: payload.meals.dem,
        },
    }
}





// This is our store(Global state) structure
export default combineReducers({
    data: mainReducer,
})
