import React, { Component } from 'react'
import {BrowserRouter, Route, Switch} from 'react-router-dom'
import {Provider} from 'react-redux'

import withAuth from "./components/utils/withAuth"
import store from './store/store'
import Login from './components/pages/Login'
import {consts, log} from './config/config'


// must import styles here to be included in main bundle js files

// order of importing is important, must import bootstrap first, cause if not , it overrides previous imported styles
// import './bootstrap.min.css';
// import './css/font-nasim.css';
import './App.css'

import Layout from "./components/pages/Layout"
import PayResult from "./components/pages/PayResult"

class App extends Component {


    render() {

        return (
            /*
            The Provider component is used to make the store available to all container components
            in the application without passing it explicitly while connect() helps us specify which
            components should get access to the state. Connect() and Provider are 2 of the most
            important pieces of React Redux middleware which are vital to the whole application’s flow.
            */

            <Provider store={store}>
                <BrowserRouter>

                    <>   {/* BrowserRouter can only have 1 element/child */}

                        <Switch>

                            <Route path="/login" component={Login} />

                            <Route path={consts.PAYMENT_CALLBACK} component={PayResult} exact/>

                            <Route path="/" component={withAuth(Layout)}/>
                            {/* without exact keyword it calls Home component in every route that starts with / */}

                            <Route component={Error}/>  {/* routes that don't exist */}

                        </Switch>

                    </>

                </BrowserRouter>
            </Provider>
        )
    }
}


export default App
