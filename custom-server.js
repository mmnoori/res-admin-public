const express = require('express')
const { createProxyMiddleware } = require('http-proxy-middleware')

const port = 3045
const env = process.env.NODE_ENV
const dev = env !== 'production'

const serverAddress = 'http://localhost:4050/'
const forwardedPort = 'x-forwarded-port'

// this server is ONLY FOR DEVELOPMENT use

const hosts = [
    {
        port: '3006',
        name: 'demo',
        staticPath: '/demo/'
    },
    {
        port: '3008',
        name: 'imo',
        staticPath: '/imo/'
    },
    {
        port: '3009',
        name: 'hezar',
        staticPath: '/hezar/'
    }
]


const server = express()


const options = {
    target: serverAddress, // target host
    changeOrigin: true, // needed for virtual hosted sites
    // router: {
    //     // when request.headers.host == 'dev.localhost:3000',
    //     // override target 'http://www.example.org' to 'http://localhost:8000'
    //     'dev.localhost:3000': 'http://localhost:8000'
    // }
    // headers: {
    //     name: 'asdasfsadagserfdhsfdgdsegdgsgfs'
    // }
}

const exampleProxy = createProxyMiddleware(options)


const staticOptions = {
    target: serverAddress,
    changeOrigin: true,
    pathRewrite:
        (path, req) => {
            // rewrite images path
            hosts.forEach((host, index) => {
                 if (req.headers[forwardedPort] === host.port) return path = path.replace('/', host.staticPath)
            })

            return path
        }
    ,
}

const staticsProxy = createProxyMiddleware(staticOptions)

// serviceworker and manifest are within react project static files
// just need to proxy images
server.use('/images', staticsProxy)

// adding name to headers for server to detect right host
// server.use('*', (req, res, next) => {
//
//     hosts.forEach((host, index) => {
//         if (req.headers[forwardedPort] === host.port) req.headers.name = host.name
//     })
//
//     next()
// }, exampleProxy)


server.use('/api/*', (req, res, next) => {

    hosts.forEach((host, index) => {
        if (req.headers[forwardedPort] === host.port) req.headers.name = host.name
    })

    next()
}, exampleProxy)



server.listen(port, '0.0.0.0', err => {
  if (err) throw err
  console.log(`> Ready on port ${port}`)
})
